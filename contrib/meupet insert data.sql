--
-- TOC entry 2689 (class 0 OID 17735)
-- Dependencies: 218
-- Data for Name: meupet_statusgroup; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.meupet_statusgroup (id, slug, name) VALUES (1, 'adopcion', 'Para Adopción');
INSERT INTO public.meupet_statusgroup (id, slug, name) VALUES (4, 'encontradas', 'Encontradas');
INSERT INTO public.meupet_statusgroup (id, slug, name) VALUES (2, 'adoptadas', 'Adoptadas');
INSERT INTO public.meupet_statusgroup (id, slug, name) VALUES (3, 'extraviadas', 'Extraviadas');

--
-- TOC entry 2685 (class 0 OID 17717)
-- Dependencies: 214
-- Data for Name: meupet_petstatus; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.meupet_petstatus (id, code, description, final, group_id, next_status_id) VALUES (1, 'Adoptada', 'Mascota adoptada', true, 2, NULL);
INSERT INTO public.meupet_petstatus (id, code, description, final, group_id, next_status_id) VALUES (2, 'Adopción', 'Mascota para adopción', false, 1, 1);
INSERT INTO public.meupet_petstatus (id, code, description, final, group_id, next_status_id) VALUES (4, 'Extraviada', 'Mascota Extraviada', false, 3, 3);
INSERT INTO public.meupet_petstatus (id, code, description, final, group_id, next_status_id) VALUES (3, 'Encontrada', 'Masc. Encontrada', true, 4, NULL);

--
-- TOC entry 2681 (class 0 OID 17689)
-- Dependencies: 210
-- Data for Name: meupet_kind; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.meupet_kind (id, kind, slug) VALUES (1, 'Canino', 'canino');
INSERT INTO public.meupet_kind (id, kind, slug) VALUES (2, 'Felino', 'felino');

--
-- TOC entry 2724 (class 0 OID 18110)
-- Dependencies: 253
-- Data for Name: meupet_raza; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (1, 'Criollo', NULL);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (2, 'Affenpinscher', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (3, 'Airedale terrier', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (4, 'Akita Americano', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (5, 'Akita Inu', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (6, 'Alano', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (7, 'Alano español', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (8, 'Alaskan Malamute ', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (9, 'American Cocker Spaniel', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (10, 'American Hairless terrier', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (11, 'American Pit bull Terrier', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (12, 'American Staffordshire Terrier ', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (13, 'American Water Spaniel ', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (14, 'Antiguo Pastor Inglés', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (15, 'Australian Terrier', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (16, 'Balkan Hound', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (17, 'Bardino (Perro majorero)', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (18, 'Basenji', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (19, 'Basset Azul de Gaseogne ', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (20, 'Basset Hound ', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (21, 'Beagle ', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (22, 'Bearded Collie ', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (23, 'Beauceron', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (24, 'Bichon frisé', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (25, 'Bichón Habanero', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (26, 'Bichón Maltés ', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (27, 'Bloodhound', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (28, 'Bobtail ', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (29, 'Border Collie ', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (30, 'Border Terrier', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (31, 'Borzoi', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (32, 'Boston Terrier ', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (33, 'Boxer ', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (34, 'Boyero Australiano', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (35, 'Boyero de Flandes', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (36, 'Braco Alemán', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (37, 'Braco de Auvernia', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (38, 'Braco de Saint Germain', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (39, 'Braco de Weimar', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (40, 'Braco francés', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (41, 'Braco húngaro', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (42, 'Braco italiano', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (43, 'Braco tirolés', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (44, 'Bretón Español', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (45, 'Bull Terrier ', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (46, 'Bulldog Americano ', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (47, 'Bulldog francés', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (48, 'Bulldog Francés ', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (49, 'Bulldog Inglés', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (50, 'Bullmastiff', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (51, 'Can de palleiro', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (52, 'Caniche ', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (53, 'Cão', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (54, 'Carlino', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (55, 'Chart Polski', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (56, 'Chihuahua', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (57, 'Chihuahueño', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (58, 'Chin Japonés', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (59, 'Chow Chow ', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (60, 'Cimarrón Uruguayo', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (61, 'Cirneco del Etna', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (62, 'Clumber spaniel', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (63, 'Cocker Spaniel Americano ', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (64, 'Cocker Spaniel Inglés', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (65, 'Collie', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (66, 'Collie barbudo', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (67, 'Crestado Chino', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (68, 'Dachshund', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (69, 'Dálmata', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (70, 'Deutsch Drahthaar', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (71, 'Doberman', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (72, 'Dogo Alemán', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (73, 'Dogo Argentino', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (74, 'Dogo de Burdeos', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (75, 'Dogo guatemalteco', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (76, 'Epagneul papillón', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (77, 'Fila Brasileño', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (78, 'Finlandés', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (79, 'Flat-Coated Retriever', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (80, 'Fox Terrier de pelo liso ', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (81, 'Fox Terrier ', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (82, 'Foxhound Americano', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (83, 'Foxhound Inglés', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (84, 'Galgo Afgano ', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (85, 'Galgo español', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (86, 'Galgo húngaro', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (87, 'Galgo inglés', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (88, 'Galgo italiano', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (89, 'Gegar colombiano', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (90, 'Gigante de los Pirineos', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (91, 'Golden Retriever ', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (92, 'Gos D Atura', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (93, 'Gran Danés', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (94, 'Greyhound', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (95, 'Grifón belga', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (96, 'Grifón de Bohemia', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (97, 'Hovawart', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (98, 'Husky Siberiano', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (99, 'Iceland Sheepdog', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (100, 'Irish Wolfhound', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (101, 'Jack Russell Terrier', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (102, 'Keeshond', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (103, 'Kelpie Australiano', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (104, 'Kerry blue terrier', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (105, 'Komondor', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (106, 'Kuvasz', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (107, 'Labrador', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (108, 'Labrador Retriever', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (109, 'Laika de Siberia Occidental', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (110, 'Laika Ruso-europeo', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (111, 'Lebrel afgano', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (112, 'Lebrel Escocés', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (113, 'Leonberger', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (114, 'Lhasa apso', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (115, 'Lobo de saarloos', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (116, 'Maltés', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (117, 'Manchester terrier', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (118, 'Mastin', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (119, 'Mastín afgano', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (120, 'Mastín de los Pirineos', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (121, 'Mastín del Pirineo', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (122, 'Mastin del Tibet', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (123, 'Mastín Español', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (124, 'Mastín inglés', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (125, 'Mastín Napolitano', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (126, 'Mastín tibetano', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (127, 'Mucuchies', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (128, 'Otterhound', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (129, 'Ovejero magallánico', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (130, 'Papillón', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (131, 'Pastor Alemán', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (132, 'Pastor Australiano', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (133, 'Pastor Belga ', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (134, 'Pastor blanco suizo', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (135, 'Pastor catalán', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (136, 'Pastor croata', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (137, 'Pastor de Brie', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (138, 'Pastor de los Pirineos', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (139, 'Pastor de los Pirineos de Cara ', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (140, 'Pastor Ganadero Australiano', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (141, 'Pastor garafiano', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (142, 'Pastor holandés', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (143, 'Pastor leonés', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (144, 'Pastor mallorquín', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (145, 'Pastor peruano Chiribaya', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (146, 'Pastor vasco', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (147, 'Pembroke Welsh Corgi', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (148, 'Pequeño Lebrel Italiano', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (149, 'Pequinés', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (150, 'Perdiguero Chesapeake Bay', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (151, 'Perdiguero de Drentse', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (152, 'Perdiguero de Pelo lido', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (153, 'Perdiguero de pelo rizado', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (154, 'Perdiguero francés', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (155, 'Perdiguero Portugués', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (156, 'Perro cimarrón uruguayo', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (157, 'Perro de agua americano', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (158, 'Perro de agua español', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (159, 'Perro de Agua Francés', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (160, 'Perro de agua irlandés', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (161, 'Perro de agua portugués', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (162, 'Perro de Montaña de los Pirineo', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (163, 'Perro dogo mallorquín', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (164, 'Perro esquimal canadiense', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (165, 'Perro pastor de las islas Shetl', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (166, 'Petit Basset Griffon', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (167, 'Phalène', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (168, 'Pinscher', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (169, 'Pinscher alemán', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (170, 'Pinscher miniatura', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (171, 'Pitbull', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (172, 'Podenco canario', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (173, 'Podenco Ibicenco', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (174, 'Podenco Portugués', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (175, 'Pointer', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (176, 'Pointer Inglés', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (177, 'Pomerania', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (178, 'Presa canario', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (179, 'Presa Mallorquin', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (180, 'Pug', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (181, 'Puli', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (182, 'Rafeiro', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (183, 'Ratón Bodeguero Andaluz', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (184, 'Ratonero mallorquín', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (185, 'Ratonero valenciano', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (186, 'Retriever de pelo rizado', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (187, 'Rhodesian Ridgeback', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (188, 'Rottweiler ', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (189, 'Rough Collie', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (190, 'Sabueso Español', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (191, 'Sabueso Hélenico', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (192, 'Sabueso Italiano', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (193, 'Sabueso Suizo', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (194, 'Saluki', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (195, 'Samoyedo ', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (196, 'San Bernardo ', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (197, 'Sato', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (198, 'Schnauzer', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (199, 'Schnauzer estándar', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (200, 'Schnauzer gigante', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (201, 'Schnauzer miniatura', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (202, 'Scottish Terrier ', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (203, 'Setter inglés', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (204, 'Setter Irlandés ', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (205, 'Shar Pei ', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (206, 'Shetland Sheepdog', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (207, 'Shiba Inu ', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (208, 'Shih Tzu', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (209, 'Siberian Husky ', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (210, 'Skye terrier', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (211, 'Spinone italiano', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (212, 'Staffordshire Bull Terrier', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (213, 'Sussex spaniel', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (214, 'Teckel', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (215, 'Terranova ', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (216, 'Terrier alemán', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (217, 'Terrier Australiano', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (218, 'Terrier brasileño', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (219, 'Terrier Checo', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (220, 'Terrier chileno', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (221, 'Terrier Escocés ', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (222, 'Terrier galés', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (223, 'Terrier Irlandés ', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (224, 'Terrier Japonés', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (225, 'Terrier Negro Ruso', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (226, 'Terrier Norfolk', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (227, 'Terrier Norwich', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (228, 'Terrier Tibetano', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (229, 'Tosa Inu', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (230, 'Weimaraner', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (231, 'West Highland White Terrier', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (232, 'Whippet', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (233, 'Xoloitzcuintle', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (234, 'Yorkshire Terrier', 1);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (235, 'Abisinio', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (236, 'Africano doméstico', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (237, 'American Curl', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (238, 'American shorthair', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (239, 'American wirehair', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (240, 'Angora turco', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (241, 'Aphrodite-s Giants', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (242, 'Australian Mist', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (243, 'Azul ruso', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (244, 'Bengala', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (245, 'Bobtail japonés', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (246, 'Bombay', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (247, 'Bosque de Noruega', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (248, 'Brazilian Shorthair', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (249, 'British Shorthair', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (250, 'Burmés', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (251, 'Burmilla', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (252, 'California Spangled', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (253, 'Ceylon', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (254, 'Chartreux', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (255, 'Cornish rex', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (256, 'Cymric', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (257, 'Deutsch Langhaar', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (258, 'Devon rex', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (259, 'Don Sphynx', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (260, 'Dorado africano', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (261, 'Europeo común', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (262, 'Gato exótico', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (263, 'German Rex', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (264, 'Habana brown', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (265, 'Himalayo', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (266, 'Khao Manee', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (267, 'Korat', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (268, 'Maine Coon', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (269, 'Manx', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (270, 'Mau egipcio', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (271, 'Munchkin', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (272, 'Ocicat', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (273, 'Ojos azules', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (274, 'Oriental', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (275, 'Oriental de pelo largo', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (276, 'Persa', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (277, 'Peterbald', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (278, 'Pixi Bob', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (279, 'Ragdoll', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (280, 'Sagrado de Birmania', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (281, 'Scottish Fold', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (282, 'Selkirk rex', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (283, 'Serengeti', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (284, 'Seychellois', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (285, 'Siamés', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (286, 'Siamés Moderno', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (287, 'Siamés Tradicional', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (288, 'Siberiano', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (289, 'Snowshoe', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (290, 'Sphynx', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (291, 'Tonkinés', 2);
INSERT INTO public.meupet_raza (id, nombre, especie_id) VALUES (292, 'Van Turco', 2);

--
-- TOC entry 2679 (class 0 OID 17673)
-- Dependencies: 208
-- Data for Name: cities_state; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.cities_state (id, code, name, abbr) VALUES (1, 1, 'Nariño', 'NA');

--
-- TOC entry 2677 (class 0 OID 17665)
-- Dependencies: 206
-- Data for Name: cities_city; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.cities_city (id, code, name, search_name, lat, lon, state_id) VALUES (1, 1, 'Pasto', 'pasto', 1.23324319999999998, 3.45345345000000004, 1);
INSERT INTO public.cities_city (id, code, name, search_name, lat, lon, state_id) VALUES (3, 2, 'Ipiales', 'ipiales', 2, 2, 1);
INSERT INTO public.cities_city (id, code, name, search_name, lat, lon, state_id) VALUES (4, 3, 'La Unión', 'la union', 4545, 845488, 1);
INSERT INTO public.cities_city (id, code, name, search_name, lat, lon, state_id) VALUES (5, 5, 'El Tambo', 'El Tambo', 1, 1, 1);
INSERT INTO public.cities_city (id, code, name, search_name, lat, lon, state_id) VALUES (6, 6, 'Chachagüí', 'chachagui', NULL, NULL, 1);
INSERT INTO public.cities_city (id, code, name, search_name, lat, lon, state_id) VALUES (7, 7, 'La Florida', 'la florida', NULL, NULL, 1);
INSERT INTO public.cities_city (id, code, name, search_name, lat, lon, state_id) VALUES (8, 8, 'Buesaco', 'buesaco', NULL, NULL, 1);
INSERT INTO public.cities_city (id, code, name, search_name, lat, lon, state_id) VALUES (9, 9, 'Los Andes Sotomayor', 'los andes sotomayor', NULL, NULL, 1);
INSERT INTO public.cities_city (id, code, name, search_name, lat, lon, state_id) VALUES (10, 10, 'Providencia', 'providencia', NULL, NULL, 1);
INSERT INTO public.cities_city (id, code, name, search_name, lat, lon, state_id) VALUES (11, 11, 'Sapuyes', 'sapuyes', NULL, NULL, 1);

--
-- TOC entry 2701 (class 0 OID 17827)
-- Dependencies: 230
-- Data for Name: adopcion_tiporelacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.adopcion_tiporelacion (id, nombre, descripcion, observaciones) VALUES (1, 'Adopcion', 'Relación de tipo adopción', '');
INSERT INTO public.adopcion_tiporelacion (id, nombre, descripcion, observaciones) VALUES (2, 'Postulación', 'Relación de tipo postulacion', '');

--
-- TOC entry 2693 (class 0 OID 17784)
-- Dependencies: 222
-- Data for Name: adopcion_clausulas; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.adopcion_clausulas (id, nombre, fecha_creacion, descripcion, observaciones, tipo, contrato_id) VALUES (2, '2', '2018-10-05 15:07:39-05', 'Nunca debe desatenderlo, hacerlo pelear o trabajar, ni sacar crías con fines económicos. No debe abandonarlo, regalarlo, cederlo, venderlo o sacrificarlo sin justificación veterinaria por enfermedad muy grave que lo obligue. No realizara amputaciones de ningún tipo por motivos estéticos. nunca debe pegarle, humillarle ni utilizarle con fines económicos.', '', '1', 1);
INSERT INTO public.adopcion_clausulas (id, nombre, fecha_creacion, descripcion, observaciones, tipo, contrato_id) VALUES (3, '3', '2018-10-05 15:08:16-05', 'Cuando se adopte un cachorro/a y este cumpla los 6 meses de edad o si es adulto y no esta esterilizado, se deberá esterilizar de manera obligatoria, siendo este el único mecanismo de controlar la sobrepoblación animal.', '', '1', 1);
INSERT INTO public.adopcion_clausulas (id, nombre, fecha_creacion, descripcion, observaciones, tipo, contrato_id) VALUES (6, '5', '2018-10-11 10:07:39-05', 'Informará a <tipo_empresa></tipo_empresa> de cualquier cambio de domicilio, de teléfono e-mail al igual que la defunción del animal adoptado o pérdida del animal, así como también de la adaptación del animal adoptado.', '', '1', 1);
INSERT INTO public.adopcion_clausulas (id, nombre, fecha_creacion, descripcion, observaciones, tipo, contrato_id) VALUES (5, '4', '2018-10-11 09:53:35-05', 'Cumplir con la legislación vigente y de nueva publicación de carácter comunitario, estatal, autonómico y local sobre animales de compañía.', '', '1', 1);
INSERT INTO public.adopcion_clausulas (id, nombre, fecha_creacion, descripcion, observaciones, tipo, contrato_id) VALUES (10, '9', '2018-10-11 10:10:37-05', 'En caso de no encontrarse capacitado algún día para continuar encargándose del animal adoptado, procederá a comunicarlo a <tipo_empresa></tipo_empresa>, quien iniciara de nuevo los trámites para la búsqueda de un nuevo adoptante. <strong>En esta situación el adoptante cuidara del animal mientras se le encuentre un nuevo hogar, o deberá pagar la guardería del animal del mes que inicie a correr inmediatamente a la entrega del animal. Parágrafo:</strong> En caso de devolución, la persona que recibe el animal verificará el estado en el cual es entregado el mismo, posteriormente lo llevará a revisión médico veterinaria a fin de que certifique el estado de salud. Lo costos de la revisión médica deben ser costeados por el adoptante que devuelve el animal, y en caso de que las condiciones de salud no sean buenas, deberá correr con el costo del tratamiento hasta que el animal se recupere.', '', '1', 1);
INSERT INTO public.adopcion_clausulas (id, nombre, fecha_creacion, descripcion, observaciones, tipo, contrato_id) VALUES (14, '13', '2018-10-11 10:24:27-05', 'Para todos los efectos legales a que haya lugar, se entenderá el presente contrato, celebrado de manera voluntaria de conformidad con los <strong>artículos 1494 y subsiguientes del Código Civil</strong>, que regula el contrato como fuente de obligaciones.', '', '1', 1);
INSERT INTO public.adopcion_clausulas (id, nombre, fecha_creacion, descripcion, observaciones, tipo, contrato_id) VALUES (1, '1', '2018-10-05 15:06:53-05', 'Ofrecer los cuidados que necesite el animal, alimentarlo, sacarlo de paseo, llevarlo a hacer sus necesidades fisiológicas, darle cobijo y tratarlo con respeto y cariño. Llevar un control sanitario visitando a su veterinario para vacunaciones, desparasitaciones y por cualquier enfermedad que se le origine. No consentir que el animal adoptado transite libremente por la vía pública, parque o campo sin estar acompañado por un adulto responsable y siempre cogido con collar y medalla. Si es la primera vez que está a cargo de un animal procurara informarse lo mejor posible de los principios básicos a tener en cuenta para ofrecerle una vida digna.', '', '1', 1);
INSERT INTO public.adopcion_clausulas (id, nombre, fecha_creacion, descripcion, observaciones, tipo, contrato_id) VALUES (13, '12', '2018-10-11 10:22:57-05', 'El adoptante debe tener en claro que en caso de incurrir en causal de maltrato animal, se encontrará incurso en la comisión de una conducta punible, de conformidad con la <strong>LEY 1774 de 2016</strong>, motivo por el cual <tipo_empresa></tipo_empresa> <nombre_corto></nombre_corto> procederá a realizar la respectiva denuncia a la autoridad competente para que el responsable responda ante las autoridades judiciales de acuerdo a la penas fijadas para las conductas determinadas.', '', '1', 1);
INSERT INTO public.adopcion_clausulas (id, nombre, fecha_creacion, descripcion, observaciones, tipo, contrato_id) VALUES (7, '6', '2018-10-11 10:08:15-05', '<tipo_empresa></tipo_empresa> <nombre_corto></nombre_corto> queda libre de cualquier responsabilidad civil o penal que pueda derivar de la tenencia o comportamiento del animal en el futuro, a partir del día de entrega del mismo al adoptante.', '', '1', 1);
INSERT INTO public.adopcion_clausulas (id, nombre, fecha_creacion, descripcion, observaciones, tipo, contrato_id) VALUES (11, '10', '2018-10-11 10:12:04-05', 'En caso de que <tipo_empresa></tipo_empresa> recupere al animal por cualquier motivo, y si el mismo presenta algún tipo de lesión o requiere tratamiento veterinario por la falta de cuidados, enfermedad o cualquier otro causado directa o indirectamente por el adoptante, por la presente este acepta <strong>asumir todos los costos en los que la <tipo_empresa></tipo_empresa> deberá incurrir para la recuperación del animal.</strong>', '', '1', 1);
INSERT INTO public.adopcion_clausulas (id, nombre, fecha_creacion, descripcion, observaciones, tipo, contrato_id) VALUES (12, '11', '2018-10-11 10:13:50-05', 'El adoptante se compromete a cuidar proteger a su mascota conforme lo reglamenta la <strong>LEY 1774 de 2016 y demás disposiciones reglamentarias de protección animal</strong>, de lo contrario y en los casos adelante señalados, el adoptante deberá cancelar a nombre de <tipo_empresa></tipo_empresa> <nombre_corto></nombre_corto> la suma de <strong>TRESCIENTOS MIL PESOS M/L ($300.000)</strong> o lo que corresponda, por daños y perjuicios.
<br><br>
<ol>
   
        <li ALIGN="justify">Por accidentarse la mascota.</li>
        <li ALIGN="justify">Por encontrarse en mal estado de salud, higiene, alimentación, y otras situaciones que afecten la integridad del animal.</li>
        <li ALIGN="justify">Por evidenciarse descuido en el trato de la mascota.</li>
    
</ol>', '', '1', 1);
INSERT INTO public.adopcion_clausulas (id, nombre, fecha_creacion, descripcion, observaciones, tipo, contrato_id) VALUES (8, '7', '2018-10-11 10:09:36-05', 'Acepta que se le realice visitas a su domicilio por los representantes de <tipo_empresa></tipo_empresa> con el fin de observar el estado de adopción, estado y condiciones del animal, reservándose <tipo_empresa></tipo_empresa> el derecho a retirar la custodia del mismo si considera que no está adecuadamente atendido o no se cumpliese con el actual contrato.', '', '1', 1);
INSERT INTO public.adopcion_clausulas (id, nombre, fecha_creacion, descripcion, observaciones, tipo, contrato_id) VALUES (9, '8', '2018-10-11 10:10:03-05', 'Siempre que <tipo_empresa></tipo_empresa> <nombre_corto></nombre_corto> decida recoger de nuevo el animal adoptado, este será entregado de buen grado por el adoptante, sin dañar su integridad física ni psíquica. Si el adoptante no consintiera en esta retirada serán por cuenta del mismo la totalidad de los gastos judiciales para la resolución del contrato de adopción y devolución del adoptado, aun en el caso de que no exista especial pronunciamiento sobre las costas.', '', '1', 1);


--
-- TOC entry 2695 (class 0 OID 17795)
-- Dependencies: 224
-- Data for Name: adopcion_contratos; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.adopcion_contratos (id, nombre, objeto, fecha_creacion, descripcion, observaciones, tipo, contrato_base_id, fundacion_id) VALUES (1, 'CONTRATO LEGAL DE ADOPCIÓN DE UN ANIMAL DE COMPAÑÍA', '1', '2018-10-05 15:02:48-05', '<strong>CARÁCTER VINCULANTE DEL CONTRATO: (…) Artículo 1602 del C.C.:</strong> “Todo contrato legalmente celebrado es una ley para los contratantes, y no puede ser invalidado sino por su consentimiento mutuo o por causas legales.” (…) Y demás disposiciones concordantes.
<br><br>
<strong>EL ADOPTANTE DECLARA:</strong>
<br>
1.  El adoptante ha leído detenidamente y declara entender las condiciones del contrato y los compromisos a que se obliga con la suscripción del presente documento.<br>
2.  Está conforme con las condiciones de adopción; que las personas que viven en su domicilio, donde vivirá el animal adoptado, han sido informadas previamente de su deseo de adoptar al mismo y todos están de acuerdo en colaborar y cumplir las condiciones del contrato como “propietarios indirectos”. Haber sido informados previamente del estado de salud, edad, carácter y características físicas del animal adoptado por parte de la Fundación.<br>
3.  El adoptante conoce que en caso de incumplir con las obligaciones, la fundación podrá exigir el cumplimiento por las vías legales que correspondan.<br>
4.  Esta adopción no responde a un impulso, sino a un acto meditado.<br><br>

<strong>REQUISITOS:</strong><br>
1.  Presentar documento de identificación oficial original y copia (ampliada al 150%)<br>
2.  Proporcionarle a la mascota un collar y placa de identificación, “ la placa es obligatoria”<br>
3.  No ceder o regalar a la mascota sin previo aviso a alguien representante de la Fundación.<br>
4.  No convertir a la mascota en agresivo que sea riesgo para los demás.<br>
5.  Hacerse responsable de los daños y perjuicios ocasionados por la mascota.<br>', '', '1', NULL, NULL);
