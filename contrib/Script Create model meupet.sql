-- SEQUENCE: public.meupet_kind_id_seq

-- DROP SEQUENCE public.meupet_kind_id_seq;

CREATE SEQUENCE public.meupet_kind_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.meupet_kind_id_seq
    OWNER TO postgres;

-- SEQUENCE: public.meupet_pet_id_seq

-- DROP SEQUENCE public.meupet_pet_id_seq;

CREATE SEQUENCE public.meupet_pet_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.meupet_pet_id_seq
    OWNER TO postgres;

-- SEQUENCE: public.meupet_petstatus_id_seq

-- DROP SEQUENCE public.meupet_petstatus_id_seq;

CREATE SEQUENCE public.meupet_petstatus_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.meupet_petstatus_id_seq
    OWNER TO postgres;

-- SEQUENCE: public.meupet_photo_id_seq

-- DROP SEQUENCE public.meupet_photo_id_seq;

CREATE SEQUENCE public.meupet_photo_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.meupet_photo_id_seq
    OWNER TO postgres;

-- SEQUENCE: public.meupet_raza_id_seq

-- DROP SEQUENCE public.meupet_raza_id_seq;

CREATE SEQUENCE public.meupet_raza_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.meupet_raza_id_seq
    OWNER TO postgres;

-- SEQUENCE: public.meupet_statusgroup_id_seq

-- DROP SEQUENCE public.meupet_statusgroup_id_seq;

CREATE SEQUENCE public.meupet_statusgroup_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.meupet_statusgroup_id_seq
    OWNER TO postgres;
        
-- Table: public.meupet_kind

-- DROP TABLE public.meupet_kind;

CREATE TABLE public.meupet_kind
(
    id integer NOT NULL DEFAULT nextval('meupet_kind_id_seq'::regclass),
    kind text COLLATE pg_catalog."default" NOT NULL,
    slug character varying(30) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT meupet_kind_pkey PRIMARY KEY (id),
    CONSTRAINT meupet_kind_kind_key UNIQUE (kind)

)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.meupet_kind
    OWNER to postgres;

-- Index: meupet_kind_kind_62009274_like

-- DROP INDEX public.meupet_kind_kind_62009274_like;

CREATE INDEX meupet_kind_kind_62009274_like
    ON public.meupet_kind USING btree
    (kind COLLATE pg_catalog."default" text_pattern_ops)
    TABLESPACE pg_default;

-- Index: meupet_kind_slug_57975180

-- DROP INDEX public.meupet_kind_slug_57975180;

CREATE INDEX meupet_kind_slug_57975180
    ON public.meupet_kind USING btree
    (slug COLLATE pg_catalog."default")
    TABLESPACE pg_default;

-- Index: meupet_kind_slug_57975180_like

-- DROP INDEX public.meupet_kind_slug_57975180_like;

CREATE INDEX meupet_kind_slug_57975180_like
    ON public.meupet_kind USING btree
    (slug COLLATE pg_catalog."default" varchar_pattern_ops)
    TABLESPACE pg_default;

-- Table: public.meupet_statusgroup

-- DROP TABLE public.meupet_statusgroup;

CREATE TABLE public.meupet_statusgroup
(
    id integer NOT NULL DEFAULT nextval('meupet_statusgroup_id_seq'::regclass),
    slug character varying(64) COLLATE pg_catalog."default" NOT NULL,
    name character varying(64) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT meupet_statusgroup_pkey PRIMARY KEY (id),
    CONSTRAINT meupet_statusgroup_slug_key UNIQUE (slug)

)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.meupet_statusgroup
    OWNER to postgres;

-- Index: meupet_statusgroup_slug_ddacb9b9_like

-- DROP INDEX public.meupet_statusgroup_slug_ddacb9b9_like;

CREATE INDEX meupet_statusgroup_slug_ddacb9b9_like
    ON public.meupet_statusgroup USING btree
    (slug COLLATE pg_catalog."default" varchar_pattern_ops)
    TABLESPACE pg_default;

-- Table: public.meupet_raza

-- DROP TABLE public.meupet_raza;

CREATE TABLE public.meupet_raza
(
    id integer NOT NULL DEFAULT nextval('meupet_raza_id_seq'::regclass),
    nombre character varying(64) COLLATE pg_catalog."default" NOT NULL,
    especie_id integer,
    CONSTRAINT meupet_raza_pkey PRIMARY KEY (id),
    CONSTRAINT meupet_raza_especie_id_21089e13_fk_meupet_kind_id FOREIGN KEY (especie_id)
        REFERENCES public.meupet_kind (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        DEFERRABLE INITIALLY DEFERRED
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.meupet_raza
    OWNER to postgres;

-- Index: meupet_raza_especie_id_21089e13

-- DROP INDEX public.meupet_raza_especie_id_21089e13;

CREATE INDEX meupet_raza_especie_id_21089e13
    ON public.meupet_raza USING btree
    (especie_id)
    TABLESPACE pg_default;


-- Table: public.meupet_petstatus

-- DROP TABLE public.meupet_petstatus;

CREATE TABLE public.meupet_petstatus
(
    id integer NOT NULL DEFAULT nextval('meupet_petstatus_id_seq'::regclass),
    code character varying(32) COLLATE pg_catalog."default" NOT NULL,
    description character varying(64) COLLATE pg_catalog."default" NOT NULL,
    final boolean NOT NULL,
    group_id integer,
    next_status_id integer,
    CONSTRAINT meupet_petstatus_pkey PRIMARY KEY (id),
    CONSTRAINT meupet_petstatus_code_key UNIQUE (code)
,
    CONSTRAINT meupet_petstatus_next_status_id_key UNIQUE (next_status_id)
,
    CONSTRAINT meupet_petstatus_group_id_5b512908_fk_meupet_statusgroup_id FOREIGN KEY (group_id)
        REFERENCES public.meupet_statusgroup (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        DEFERRABLE INITIALLY DEFERRED,
    CONSTRAINT meupet_petstatus_next_status_id_abb3445c_fk_meupet_petstatus_id FOREIGN KEY (next_status_id)
        REFERENCES public.meupet_petstatus (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        DEFERRABLE INITIALLY DEFERRED
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.meupet_petstatus
    OWNER to postgres;

-- Index: meupet_petstatus_code_30b7556f_like

-- DROP INDEX public.meupet_petstatus_code_30b7556f_like;

CREATE INDEX meupet_petstatus_code_30b7556f_like
    ON public.meupet_petstatus USING btree
    (code COLLATE pg_catalog."default" varchar_pattern_ops)
    TABLESPACE pg_default;

-- Index: meupet_petstatus_group_id_5b512908

-- DROP INDEX public.meupet_petstatus_group_id_5b512908;

CREATE INDEX meupet_petstatus_group_id_5b512908
    ON public.meupet_petstatus USING btree
    (group_id)
    TABLESPACE pg_default;

-- Table: public.meupet_pet

-- DROP TABLE public.meupet_pet;

CREATE TABLE public.meupet_pet
(
    id integer NOT NULL DEFAULT nextval('meupet_pet_id_seq'::regclass),
    created timestamp with time zone NOT NULL,
    modified timestamp with time zone NOT NULL,
    chip_id bigint,
    condicion_corporal character varying(1) COLLATE pg_catalog."default" NOT NULL,
    peso numeric(5,2) NOT NULL,
    color character varying(250) COLLATE pg_catalog."default",
    edad character varying(250) COLLATE pg_catalog."default",
    name character varying(50) COLLATE pg_catalog."default" NOT NULL,
    description character varying(500) COLLATE pg_catalog."default" NOT NULL,
    size character varying(2) COLLATE pg_catalog."default" NOT NULL,
    sex character varying(2) COLLATE pg_catalog."default" NOT NULL,
    profile_picture character varying(100) COLLATE pg_catalog."default" NOT NULL,
    published boolean NOT NULL,
    request_sent timestamp with time zone,
    request_key character varying(40) COLLATE pg_catalog."default" NOT NULL,
    active boolean NOT NULL,
    slug character varying(50) COLLATE pg_catalog."default" NOT NULL,
    city_id integer NOT NULL,
    fundacion integer NOT NULL,
    kind_id integer,
    owner_id integer NOT NULL,
    raza_id integer,
    status_id integer,
    CONSTRAINT meupet_pet_pkey PRIMARY KEY (id),
    CONSTRAINT meupet_pet_chip_id_key UNIQUE (chip_id)
,
    CONSTRAINT meupet_pet_name_owner_id_a1c29243_uniq UNIQUE (name, owner_id)
,
    CONSTRAINT meupet_pet_slug_key UNIQUE (slug)
,
    CONSTRAINT meupet_pet_city_id_4aa9fc1d_fk_cities_city_id FOREIGN KEY (city_id)
        REFERENCES public.cities_city (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        DEFERRABLE INITIALLY DEFERRED,
    CONSTRAINT meupet_pet_fundacion_f3a86ab3_fk_users_fundacion_id FOREIGN KEY (fundacion)
        REFERENCES public.users_fundacion (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        DEFERRABLE INITIALLY DEFERRED,
    CONSTRAINT meupet_pet_kind_id_60a68267_fk_meupet_kind_id FOREIGN KEY (kind_id)
        REFERENCES public.meupet_kind (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        DEFERRABLE INITIALLY DEFERRED,
    CONSTRAINT meupet_pet_owner_id_4255b3a7_fk_users_ownerprofile_id FOREIGN KEY (owner_id)
        REFERENCES public.users_ownerprofile (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        DEFERRABLE INITIALLY DEFERRED,
    CONSTRAINT meupet_pet_raza_id_92c5325a_fk_meupet_raza_id FOREIGN KEY (raza_id)
        REFERENCES public.meupet_raza (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        DEFERRABLE INITIALLY DEFERRED,
    CONSTRAINT meupet_pet_status_id_c64f7c64_fk_meupet_petstatus_id FOREIGN KEY (status_id)
        REFERENCES public.meupet_petstatus (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        DEFERRABLE INITIALLY DEFERRED
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.meupet_pet
    OWNER to postgres;

-- Index: meupet_pet_city_id_4aa9fc1d

-- DROP INDEX public.meupet_pet_city_id_4aa9fc1d;

CREATE INDEX meupet_pet_city_id_4aa9fc1d
    ON public.meupet_pet USING btree
    (city_id)
    TABLESPACE pg_default;

-- Index: meupet_pet_fundacion_f3a86ab3

-- DROP INDEX public.meupet_pet_fundacion_f3a86ab3;

CREATE INDEX meupet_pet_fundacion_f3a86ab3
    ON public.meupet_pet USING btree
    (fundacion)
    TABLESPACE pg_default;

-- Index: meupet_pet_kind_id_60a68267

-- DROP INDEX public.meupet_pet_kind_id_60a68267;

CREATE INDEX meupet_pet_kind_id_60a68267
    ON public.meupet_pet USING btree
    (kind_id)
    TABLESPACE pg_default;

-- Index: meupet_pet_owner_id_4255b3a7

-- DROP INDEX public.meupet_pet_owner_id_4255b3a7;

CREATE INDEX meupet_pet_owner_id_4255b3a7
    ON public.meupet_pet USING btree
    (owner_id)
    TABLESPACE pg_default;

-- Index: meupet_pet_raza_id_92c5325a

-- DROP INDEX public.meupet_pet_raza_id_92c5325a;

CREATE INDEX meupet_pet_raza_id_92c5325a
    ON public.meupet_pet USING btree
    (raza_id)
    TABLESPACE pg_default;

-- Index: meupet_pet_slug_538b752e_like

-- DROP INDEX public.meupet_pet_slug_538b752e_like;

CREATE INDEX meupet_pet_slug_538b752e_like
    ON public.meupet_pet USING btree
    (slug COLLATE pg_catalog."default" varchar_pattern_ops)
    TABLESPACE pg_default;

-- Index: meupet_pet_status_id_c64f7c64

-- DROP INDEX public.meupet_pet_status_id_c64f7c64;

CREATE INDEX meupet_pet_status_id_c64f7c64
    ON public.meupet_pet USING btree
    (status_id)
    TABLESPACE pg_default;

-- Table: public.meupet_photo

-- DROP TABLE public.meupet_photo;

CREATE TABLE public.meupet_photo
(
    id integer NOT NULL DEFAULT nextval('meupet_photo_id_seq'::regclass),
    image character varying(100) COLLATE pg_catalog."default" NOT NULL,
    pet_id integer NOT NULL,
    CONSTRAINT meupet_photo_pkey PRIMARY KEY (id),
    CONSTRAINT meupet_photo_pet_id_52b849b1_fk_meupet_pet_id FOREIGN KEY (pet_id)
        REFERENCES public.meupet_pet (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        DEFERRABLE INITIALLY DEFERRED
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.meupet_photo
    OWNER to postgres;

-- Index: meupet_photo_pet_id_52b849b1

-- DROP INDEX public.meupet_photo_pet_id_52b849b1;

CREATE INDEX meupet_photo_pet_id_52b849b1
    ON public.meupet_photo USING btree
    (pet_id)
    TABLESPACE pg_default;
