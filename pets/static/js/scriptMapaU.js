const cargarpuntos = () => {
	$('#id_nombre').attr("readonly","readonly");
	$('#id_lat').attr("readonly","readonly");
	$('#id_lng').attr("readonly","readonly");
	/*$('#proxv').css("display", "none");	
	$("label[for='proxv']").css("display", "none");	
	$('#fot').css({display:'none'});
	$("label[for='fot']").css({display:'none'});*/
	//$("label[for='categ']").append('<a href="/gmaps/categoria">Categoria</a>');
	$('#categ').prepend('<a href="/gmaps/categoria">Categoria</a>');	
	var base = JSON.parse($("#dataq").text());
	var mapasLayer=[
			'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
			'https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png',
			'http://tile.thunderforest.com/cycle/{z}/{x}/{y}.png',
			'https://tiles.wmflabs.org/hikebike/{z}/{x}/{y}.png',
			'http://tiles.wmflabs.org/hillshading/{z}/{x}/{y}.png',
			'https://tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png',
			'https://tiles.wmflabs.org/osm-no-labels/{z}/{x}/{y}.png',
			'http://a.tile.stamen.com/toner/{z}/{x}/{y}.png',
			'http://c.tile.stamen.com/watercolor/{z}/{x}/{y}.jpg',
			'http://tile.thunderforest.com/transport/{z}/{x}/{y}.png',
			'http://tile.thunderforest.com/landscape/{z}/{x}/{y}.png',
			'http://tile.thunderforest.com/outdoors/{z}/{x}/{y}.png',
			'http://tile.memomaps.de/tilegen/{z}/{x}/{y}.png',
			'https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png',
			'https://cartodb-basemaps-{s}.global.ssl.fastly.net/dark_all/{z}/{x}/{y}.png',
			'https://tile.waymarkedtrails.org/hiking/{z}/{x}/{y}.png',
			'https://tile.waymarkedtrails.org/cycling/{z}/{x}/{y}.png',
			'https://tile.waymarkedtrails.org/riding/{z}/{x}/{y}.png',
			'http://www.openptmap.org/tiles/{z}/{x}/{y}.png'
	];

	/*$('#estadio').change(function() {
		showbtnFileDate();
	});*/


	const map = L.map('mapa').setView([1.2095000000000482, -77.27667999999994],10);
	L.tileLayer(mapasLayer[5]).addTo(map);
	L.control.scale().addTo(map);
	map.locate({enableHighAccuracy: true});

	const coordenadas=[1.2095000000000482, -77.27667999999994];
	var customIcon = new L.Icon({
	  iconUrl: '/static/img/pasto.png',
	  iconSize: [30, 30],
	  iconAnchor: [15, 35]
	});
	var marcador=L.marker(coordenadas,
		{
		title:"Ciudad de Pasto",
		draggable:false,
		icon:customIcon
		}
	);
	marcador.bindPopup("<i>Pasto, Nariño</i>").addTo(map);
	///////////////////////////////////////////////////////////////////////////
	var searchControl=L.esri.Geocoding.geosearch().addTo(map);
	var results = new L.layerGroup().addTo(map);
	searchControl.on('results', function(data){
		//location.reload();
		map.closePopup();
		results.clearLayers();
		for(var i=data.results.length - 1;i>=0;i--){
			var nombre=data.results[i].text;
			var lati=data.results[i].latlng.lat;
			var longi=data.results[i].latlng.lng;
			$('#id_nombre').val(nombre);			
			$('#id_lat').val(lati);
			$('#id_lng').val(longi);
			var pencontrado=L.marker(data.results[i].latlng);		
			results.addLayer(pencontrado);		
			var elpunto=lati.toString() + longi.toString();			
			pencontrado.bindPopup(buscarPunto(elpunto)).addTo(map);
			pencontrado.openPopup();						
		}
	});
	/////////////////////////////////////////////////////////////////////////
	function buscarPunto(elpunto){
	var editapr="";
	var desmarcar="";
	var mensaje="Puedes marcarlo";
	var esta="PENDIENTE";
	var visibilidad='visible';
	var id="";
		base.forEach(location => {
				if(elpunto==location.fields.lat + location.fields.lng){
					visibilidad='hidden';
					id=location.pk;
					if(location.fields.estado){
						esta="VISITADO";						
					}
					mensaje="Este sitio esta marcado y se sencuentra " + esta;
				}   
		});
		
		if(visibilidad=='hidden'){
			editarp="<br><a id='btnedit' href='/gmaps/editar/"+ id.toString() + "/"+ esta + "/'>Editar</a>";
			desmarcar="<br><a id='btneli' href='/gmaps/eliminar/"+ id.toString() + "/'>Desmarcar</a>";			
		}else{
			editarp="";			
		}
		$('#consulta').css('visibility', visibilidad);
		return mensaje + editarp + desmarcar; 
	}

	$( "#proxv" ).datepicker({dateFormat: 'dd/mm/yy'});	
	$( "#fecf" ).datepicker({dateFormat: 'dd/mm/yy'});	

	$('.controlgroup-vertical').controlgroup({
      "direction": "vertical"
    });


}
	
/*
function showbtnFileDate(){
	if ($('#estadio').val()=='False') {
		$('#proxv').css({display:'block'});
		$("label[for='proxv']").css({display:'block', float:'left'});	
		$('#fot').css({display:'none'});
		$("label[for='fot']").css({display:'none'});			
	}else if ($('#estadio').val()=='True') {
		$('#proxv').css("display", "none");
		$("label[for='proxv']").css("display", "none");	
		$('#fot').css({display:'block'});
		$("label[for='fot']").css({display:'block', float:'left'});
	}else{
		$('#proxv').css("display", "none");
		$("label[for='proxv']").css("display", "none");	
		$('#fot').css({display:'none'});
		$("label[for='fot']").css({display:'none', float:'left'});

	}	
	
}*/

window.addEventListener('load',cargarpuntos)
