const cargarpuntos = () => {
	/**************************Datos db ***********************************/
	var puntos = JSON.parse($("#datap").text());
	var categs = JSON.parse($("#datac").text());
	var catubs = JSON.parse($("#datacu").text());
	/**************************Inicio de variables*************************/
	$( "#fotopunto" ).css("display", "none");	
	var losPuntos=[];
	var losPuntosV=[];
	var losPuntosP=[];
	//var categorias=['Visitado','Pendiente'];
	var categories=[];
	var listCategorias=[];
	/***---MAPA---*******************Token*********************************/
	L.mapbox.accessToken = 'pk.eyJ1IjoiYnlsbGFycmVhbCIsImEiOiJjanpvZmF6aXUwMmVyM21ubWc4Y3pqNWZmIn0.2rVfgDMO07ZXgxeLJvxzvw';
	/***---MAPA---*******************Punto de inicio y layer Mapbox********/
	var mapP = L.mapbox.map('mapaP')
	    .setView([1.2095000000000482, -77.27667999999994], 8)
	    .addLayer(L.mapbox.styleLayer('mapbox://styles/byllarreal/cjzq3dllh2ndo1clfb2ipbn6p'));
	 /***---MAPA---*******************Coordenadas ciudad Pasto*************/	
	const coordenadas=[1.2095000000000482, -77.27667999999994];
	 /***---MAPA---*******************Markers******************************/	
	var customIcon = new L.Icon({
	  iconUrl: '/static/img/pasto.png',
	  iconSize: [30, 30],
	  iconAnchor: [15, 35]
	});

	var marcador1=new L.Icon({
		iconUrl:'/static/img/visitado.png',
		iconSize: [25, 30],
	  	iconAnchor: [8, 28]
	});
	var marcador2=new L.Icon({
		iconUrl:'/static/img/pendiente.png',
		iconSize: [25, 30],
	  	iconAnchor: [8, 28]
	});

	var marcador=L.marker(coordenadas,
		{
		title:"Ciudad de Pasto",
		draggable:false,
		icon:customIcon
		}
	);
	

	/***---MAPA---********************Popup ciudad Pasto******************/	
	marcador.bindPopup("<i>Pasto, Nariño</i>").addTo(mapP);
	/***---MAPA---*******************Recorrido Array de puntos a marcar***/	
		puntos.forEach(punto => {       
		var esta='PENDIENTE';
        var ic=marcador2;
        if(punto.fields.estado){
        	esta='VISITADO';
        	ic=marcador1;
        }
        let puntosData = {
            position:{lat:parseFloat(punto.fields.lat), lng:parseFloat(punto.fields.lng)},
            name:punto.fields.nombre,            
            state:esta,
            icono:ic,
            pk:punto.pk,
            fechaini:punto.fields.fecha_inicial,
            fechafin:punto.fields.fecha_final,
            cantidad:punto.fields.cantidad,
            imag:punto.fields.foto,
            show:true
        }            
        losPuntos.push(puntosData);              
        if(punto.fields.estado){
        	losPuntosV.push(puntosData);              
        }else{
        	losPuntosP.push(puntosData);              
        }
    });
	/***---MAPA---*******************Adición de puntosn en el mapa************/		
	var results = new L.layerGroup(marcar(losPuntos)).addTo(mapP);
	/**********************Recorrido categorias*******************************/
	categs.forEach(categ =>{
		let categsData = {
			cat:categ.fields.nombre,
			peca:categ.pk,
			est:true
		}
		categories.push(categsData);
	/**********************Adición categorias al menu*******************************/
		var nameid="";
		var adicionar=false;
		catubs.forEach(catu=>{
			if (categ.pk==catu.fields.category) {adicionar=true;}
		});
		if (adicionar) {
			nameid=categ.fields.nombre;
			nameid=nameid.replace(/ /g, "");
			let $li = $('<li />',{
				id:'li' + nameid
			});

			let $input= $('<input />',{
				id:nameid,
				type:"checkbox",
				value: categ.pk,
				checked:'checked',
				class:'itemsc'
			});

			let $label = $('<label />', {
	    		for:nameid,    		
	    		text:categ.fields.nombre 
			});
				
			$('#checkboxes').append($li);
			$('#li' + nameid).append($input);		
			$("#" + nameid).after($label);
			$("#" + nameid).on('click', function(){		
				desmarcaradio();
				showhide();			
			});
		}

	});
	/**********************Recorrido tabla relación Categorias-Ubicación************/
	catubs.forEach(catub =>{
		let catubsData = {
			cat:catub.fields.category,
			loc:catub.fields.location,
			est:true
		}
		listCategorias.push(catubsData);
	});
	/**********************Funciones Marcar y Mostrar imagen*************************/
	function marcar(losPuntos){
		losPuntos.forEach(punto =>{
			var franjatitle="";
				var marcador=L.marker([punto.position.lat, punto.position.lng],
					{
						title:punto.name,
						draggable:false,
						icon:punto.icono
					}
					);
				if (punto.state=='VISITADO') {
					marcador.on('click', function(){
						mostrarImg(punto.imag,punto.name,punto.fechaini,punto.fechaini,punto.cantidad);						
					});					
						franjatitle={
						titulo:"<i>"+punto.name+ ":" +punto.state+" del: "+punto.fechaini+" al: "+punto.fechafin+"</i>",
						color:"#62AB48" 
					}
				}else{
						 franjatitle={
						titulo:"<i>"+punto.name+ ":" +punto.state+" del: "+punto.fechaini+" al: "+punto.fechafin+"</i>",
						color:"#BA0C09"
					}
				}				
				marcador.addTo(mapP);
				marcador.on('mouseover', function(){
					$('#titleinfo').css('background',franjatitle.color);
					$('#titleinfo').html(franjatitle.titulo);				
				});
				marcador.on('mouseout', function(){
						$('#titleinfo').css('background',"#000");
						$('#titleinfo').empty();	
				});	
				$(marcador._icon).attr('id', punto.pk);	
				$(marcador._icon).addClass(punto.state);			
		});		
	}
	
	function mostrarImg(img,sitio,del,al,cant){			
		var descripcion="";
		if (img=="" || img=="null") {
			$('#fotomodal').html("<img src='/static/img/busv.png' width='570' height='400'>");	
		}else{
			$('#fotomodal').html("<img src='../../media/"+img+"' width='570' height='400'>");
		}
			$('#exampleModal').modal('toggle');		
			$('#exampleModalLabel').html("<i>"+sitio+"</i>");
			descripcion='En la visita del '+del+' al: '+al+' a este municipio se realizo la labor a '+cant+' mascotas'
			$('#footerinfo').html("<i>"+descripcion+"</i>");	
		
	};
	

/**********************Acción de Filtros de Estado y Categorias*************************/
var visited=true;
var pending=true;

$('#VISITADO').on('click', function(){
		visited=$("#VISITADO").is(":checked");		
		showhide();		
});
$('#PENDIENTE').on('click', function(){
		pending=$("#PENDIENTE").is(":checked");		
		showhide();	
});
$('#all').on('click', function(){
	$('.itemsc').prop('checked',true);
	showhide();
});

function desmarcaradio(){
	$('#all').prop('checked',false);
}
 

function showhide(){
	var checksboxs = $("input[class='itemsc']");
	var chekeados=[];
	var mostrar=[];
	categories.forEach(catge =>{
		var nomcat=catge.cat;
		nomcat=nomcat.replace(/ /g, "");
		var chuleado=$("#" + nomcat).is(":checked");
		if (chuleado) {chekeados.push(catge.peca);}
	});
	if (chekeados.length!=0) {

		if (chekeados.length==checksboxs.length) {
			$('#all').prop('checked',true);
		}
		losPuntos.forEach(punto =>{					
			punto.show=true;			
		});	

		chekeados.forEach(chekeado =>{
			listCategorias.forEach(catego =>{
				if (chekeado==catego.cat) {
					var esta=false;
					mostrar.forEach(most =>{										
						if (most==catego.loc) {
							esta=true;
						}
					}); if (!esta) {						
							mostrar.push(catego.loc);
						}
				}
			});
		});	
		mostrar.forEach(most =>{
			losPuntos.forEach(punto =>{
				if (most!=punto.pk) {
					if (punto.show) {
						$('#' + punto.pk).fadeOut('slow');
						punto.show=false;
					}				
				}
			});		
		});
		mostrar.forEach(most =>{
			var laclase=$('#' + most).attr('class');			
			if(laclase=='leaflet-marker-icon leaflet-zoom-animated leaflet-interactive VISITADO'){				
				if (visited) {
					$('#' + most).fadeIn('slow');
				}else{
					$('#' + most).fadeOut('slow');
				}
			}else if(laclase=='leaflet-marker-icon leaflet-zoom-animated leaflet-interactive PENDIENTE'){
				if (pending) {
					$('#' + most).fadeIn('slow');
				}else{
					$('#' + most).fadeOut('slow');
				}
			}
						
		});
	}else{
		losPuntos.forEach(punto =>{					
			$('#' + punto.pk).fadeOut('slow');
			punto.show=false;
		});		
	}	

}


}

window.addEventListener('load',cargarpuntos);


/*********************************----FIN CODIGO----*******************************************/
