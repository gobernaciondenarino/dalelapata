from .models import OwnerProfile, Fundacion


def users_count(request):
    return {
        'users_count': OwnerProfile.objects.count()
    }

def fundacions_count(request):
    return {
        'fundacions_count': Fundacion.objects.count()
    }
