from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.utils.translation import ugettext_lazy as _
from password_reset.forms import PasswordRecoveryForm, PasswordResetForm

from apps.users.models import OwnerProfile, Fundacion


class LoginForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs.update({'autofocus': 'autofocus'})
        self.fields['username'].widget.attrs.update({'class': 'form-control'})
        self.fields['password'].widget.attrs.update({'class': 'form-control'})


class UserForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['email'].required = True
        self.fields['fundacion'].required = False
        self.fields['rol'].required = True
        self.fields['tipo_identificacion'].required = True
        self.fields['num_identificacion'].required = True
        self.fields['facebook'].help_text = _(
            'Click <a href="#" data-toggle="modal" data-target="#ajuda-facebook">'
            'Aqui</a> para obtener ayuda como llenar este campo.')
        self.fields['phone'].widget.attrs.update({'class': 'form-control'})

class FundacionForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(FundacionForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()

        self.fields['tipo_identificacion'].required = True
        self.fields['num_identificacion'].required = True
        self.fields['tipo_empresa'].required = True
        self.fields['nombre_corto'].required = True        
        self.fields['email'].required = True
        self.fields['telefono'].required = True
        self.fields['razon_social'].required = True

        #self.fields['num_identificacion'].help_text = _('Digite el numero de la forma xxxxxxxxx-x')
        #self.fields['contrato_base'].widget.attrs.update({'class': 'form-control'})

def _build_choice_field(label, choices=None, required=False):
    empty_choice = (('', '------------'),)
    field = forms.ChoiceField(
        widget=forms.Select(attrs={'class': 'form-control'}),
        label=label,
        choices=empty_choice,
        required=required
    )
    if choices:
        field.choices += choices
    return field

class RegisterForm(UserForm):
    password1 = forms.CharField(label=_('Password'), widget=forms.PasswordInput)
    password2 = forms.CharField(label=_('Confirmar Password'), widget=forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        super(RegisterForm, self).__init__(*args, **kwargs)
        acento = "àèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÿÄËÏÖÜŸçÇßØøÅåÆæœ"
        self.fields['first_name'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': _('Ej: Pepito , Pepito Fulano, Pepito F.'), 
            'pattern':'^[a-zA-z'+acento+']{3,15}(\s{1}([a-zA-z'+acento+']{3,15}|[a-zA-z'+acento+']{1,2}.)(\s{1}[a-zA-z'+acento+']{3,15})?)?$', 
            'title':'Ingrese nombres validos, Ej: Pepito '
        })

        self.fields['last_name'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': _('Ej: Arcos , Arcos Torres, Arcos T.'), 
            'pattern':'^[a-zA-z'+acento+']{2,15}(\s{1}([a-zA-z'+acento+']{2,15}|[a-zA-z'+acento+']{1,2}.)(\s{1}[a-zA-z'+acento+']{2,15})?(\s{1}[a-zA-z'+acento+']{2,15})?)?$', 
            'title':'Ingrese apellidos validos, Ej: Arcos '
        })

        self.fields['email'].widget.attrs.update({'class': 'form-control'})
        #self.fields['username'].widget.attrs.update({'class': 'form-control'})
        self.fields['facebook'].widget.attrs.update({'class': 'form-control'})
        self.fields['password1'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': _('Minimo 8 caracteres'), 
            'pattern': '^.{8,}$', 
            'title':'Ingrese una contraseña con 8 caracteres como minimo'
        })
        self.fields['password2'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': _('Minimo 8 caracteres'), 
            'pattern': '^.{8,}$', 
            'title':'Ingrese una contraseña con 8 caracteres como minimo'
        })
        self.fields['facebook'].widget.attrs.update(
            {'placeholder': _('Ingresa la url completa de su perfil en facebook.')})

        #self.fields['username'].help_text = _('Requerido 30 caracteres o menos. '
        #                                      'Solo letras, numeros y @/./+/-/_.')
        self.fields['fundacion'].widget.attrs.update({
            'class': 'form-control',
            'title':'Seleccione tienente responsable sin fundación cuando solo está registrando a su compañero animal'
        })

        #self.fields['rol'] = _build_choice_field(_('Rol'), required=True)
        #self.fields['rol']: forms.Select(attrs={'class': 'form-control'})
        self.fields['rol'].widget.attrs.update({'class': 'form-control'})

        #self.fields['tipo_identificacion']: forms.Select(attrs={'class': 'form-control'})
        self.fields['tipo_identificacion'].widget.attrs.update({'class': 'form-control'})
        self.fields['num_identificacion'].widget.attrs.update({'class': 'form-control'})

    class Meta:
        model = OwnerProfile
        fields = ('first_name', 'last_name', 'email', 'facebook', 'phone', 'password1', 'password2', 'fundacion', 
                  'rol', 'tipo_identificacion', 'num_identificacion')
        widgets = {
            'num_identificacion': forms.TextInput(attrs={
                'class': 'form-control', 
                'placeholder': _('Ej: 1080232432, 50640123'), 
                'pattern':'^(([1-9]\d{3})|([1-9]|[1-9][0-9]))\d{3}\d{3}$', 
                'title':'Ingrese un numero de identificación valido sin espacios o puntos'

            }),     
        }

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(_("Las contraseñas no coinciden."))
        return password2

    def save(self, commit=True):
        user = super(RegisterForm, self).save(commit=False)
        user.username = user.email
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user
import datetime
class RegisterFormFund(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(RegisterFormFund, self).__init__(*args, **kwargs)
        self.fields['tipo_identificacion'].widget.attrs.update({'class': 'form-control'})
        self.fields['num_identificacion'].widget.attrs.update({
            'class': 'form-control', 
            'placeholder': _('Ej: 1080232432-2 , 901234323-2'), 
            'pattern':'^([1-9]([0-9]{0,8}))\d{3}\d{3}\-\d{1}$', 
            'title':'Ingrese un numero de identificación valido sin espacios o puntos, de la forma xxxxxx-x'
        })
        self.fields['nombre_corto'].widget.attrs.update({'class': 'form-control'})
        self.fields['razon_social'].widget.attrs.update({'class': 'form-control'})
        #self.fields['fecha_fundacion'].widget.attrs.update({'class': 'form-control'})
        self.fields['email'].widget.attrs.update({'class': 'form-control'})
        self.fields['telefono'].widget.attrs.update({'class': 'form-control'})
        self.fields['logo'].widget.attrs.update({'class': 'form-control'})
        self.fields['facebook'].widget.attrs.update({'class': 'form-control'})
        self.fields['twitter'].widget.attrs.update({'class': 'form-control'})
        #self.fields['contrato_base'].widget.attrs.update({'class': 'form-control'})
        self.fields['facebook'].widget.attrs.update(
            {'placeholder': _('Ingrese la direccion completa de la fundacion en Facebook')})

    class Meta:
        model = Fundacion
        fields = ('tipo_identificacion', 'num_identificacion', 'tipo_empresa', 'logo', 'nombre_corto', 'razon_social', 'fecha_fundacion',
                  'email', 'telefono', 'facebook','twitter',)
        widgets = {
            'nombre_corto': forms.TextInput(attrs={'class': 'form-control', 'placeholder': _('Nombre Corto de la organización')}),
            'razon_social': forms.Textarea(attrs={
                'class': 'form-control',
                'placeholder': _(
                   "Razon social de la empresa")
            }),
            'fecha_fundacion': forms.SelectDateWidget(years=range(datetime.date.today().year, 1960, -1), attrs={'class': 'form-control'}),    
            'tipo_empresa': forms.Select(attrs={'class': 'form-control'}),         
        }

    def clean_profile_picture(self):
        img = self.cleaned_data.get('logo', False)
        if img and img.size > 8 * 1024 * 1024:
            raise forms.ValidationError(_('Tamaño de imagen mayor que 8MB'))
        return img

class UpdateFundacionForm(FundacionForm):
    class Meta:
        model = Fundacion
        fields = ('nombre_corto','razon_social', 'email', 'telefono', 'logo', 'facebook', 'twitter',)

    def __init__(self, *args, **kwargs):
        super(UpdateFundacionForm, self).__init__(*args, **kwargs)
        self.helper.add_input(Submit('submit', _('Save Changes')))

    def save(self, commit=True):
        self.instance.is_information_confirmed = True
        super(UpdateFundacionForm, self).save()
        

class UpdateUserForm(UserForm):
    class Meta:
        model = OwnerProfile
        fields = ('first_name', 'last_name', 'email', 'facebook', 'phone',)

    def __init__(self, *args, **kwargs):
        super(UpdateUserForm, self).__init__(*args, **kwargs)
        self.helper.add_input(Submit('submit', _('Save Changes')))

    def save(self, commit=True):
        self.instance.is_information_confirmed = True
        super(UpdateUserForm, self).save()


class UsersPasswordRecoveryForm(PasswordRecoveryForm):
    def __init__(self, *args, **kwargs):
        super(UsersPasswordRecoveryForm, self).__init__(*args, **kwargs)
        self.fields['username_or_email'].label = ''
        self.helper = FormHelper()
        self.helper.add_input(Submit('recover', _('Recover password')))


class UsersPasswordResetForm(PasswordResetForm):
    def __init__(self, *args, **kwargs):
        super(UsersPasswordResetForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit('recover', _('Recover password')))
