from django.db import models
from django.contrib.auth.models import AbstractUser
from apps.users.validators import *
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.core.validators import MaxValueValidator, MinValueValidator
from django.utils import timezone
from django.utils.timezone import now

import re
from django.core.validators import RegexValidator

v_cc = re.compile(r'^(([1-9]\d{3})|([1-9]|[1-9][0-9]))\d{3}\d{3}$', re.IGNORECASE)
v_nit = re.compile(r'^([1-9]([0-9]{0,8}))\d{3}\d{3}\-\d{1}$', re.IGNORECASE)
v_firts_name = re.compile(r'^[a-zA-z]{3,15}(\s{1}([a-zA-z]{3,15}|[a-zA-z]{1,2}.)(\s{1}[a-zA-z]{3,15})?)?$', re.IGNORECASE)

class Fundacion(models.Model):
    RUT = 'RUT'
    NIT = 'NIT'
    TIPO_ID = (
        (RUT, _('RUT')),
        (NIT, _('NIT')),
    )
    FUNDACION = '1'
    CENTRO_VETERINARIO = '2'
    CBA = '3'
    TIPO_EMPRESA = (
        (FUNDACION, _('Fundacion')),
        (CENTRO_VETERINARIO, _('Centro Veterinario')),
        (CBA, _('Centro de bienestar animal')),
    )
    tipo_identificacion = models.CharField(max_length=3,
                                           choices=TIPO_ID)
    num_identificacion = models.CharField(unique=True,max_length=17, validators=[RegexValidator(regex=v_nit, message='Ingrese un numero valido de identificacion xxxxxxxx-x sin puntos')])
    nombre_corto = models.CharField(max_length=100)
    razon_social = models.TextField(max_length=1000)
    descripcion = models.TextField(blank=True, null=True)
    fecha_fundacion = models.DateField(blank=True, null=True, default=now)
    email = models.EmailField(unique=True, max_length=250)
    telefono = models.CharField(max_length=250, blank=True, null=True)
    direccion = models.CharField(max_length=250, blank=True, null=True)
    logo = models.ImageField(upload_to='fundacion_profiles',
                                        help_text=_('Maximo tamaño 8mb'), blank=True, null=True)
    tipo_empresa = models.CharField(max_length=1,
                                           choices=TIPO_EMPRESA)
    facebook = models.URLField(max_length=250, blank=True, null=True,
                               validators=[])
    twitter = models.URLField(max_length=250, blank=True, null=True,
                               validators=[])
    def get_tipo_empresa(self):
        return dict(self.TIPO_EMPRESA).get(self.tipo_empresa)

    def get_prefijo(self):
        if self.tipo_empresa=='1' :
            return str("la")
        return str("el")

    def __str__(self):
        return self.nombre_corto

CC = 'CC'
TI = 'TI'
TIPO_ID = (
    (CC, _('Cedula ciudadania')),
    (TI, _('Tarjeta identidad')),
)

class OwnerProfile(AbstractUser):
    RESPONSABLE = '1'
    VINCULADO = '2'
    BRIGADISTA = '3'
    #TENIENTE = '4'
    TIPO_ROL = (
        (RESPONSABLE, _('Responsable')),       
        (VINCULADO, _('Vinculado')),
        (BRIGADISTA, _('Brigadista')),
        #(TENIENTE, _('Teniente Responsable')),
    )
    rol = models.CharField(max_length=4, choices=TIPO_ROL, blank=True)
    tipo_identificacion = models.CharField(max_length=2,
                            choices=TIPO_ID,
                            blank=True)

    #self._meta.get_field('first_name')._validators = RegexValidator(regex=v_cc, message='Escriba un nombre valido, ej: pepito, pepito fulano, pepito f.')

    #num_identificacion = models.CharField('Numero de identificación', max_length=13)
    num_identificacion = models.IntegerField('Numero de identificación', validators=[RegexValidator(regex=v_cc, message='Ingrese un numero de identificacion valido')])
    is_information_confirmed = models.BooleanField(default=False)
    facebook = models.URLField(max_length=250, blank=True, null=True,
                               validators=[validate_facebook_url])
    phone = models.CharField('Telefono de contacto',  max_length=30, blank=True)
    direccion = models.CharField(max_length=250, blank=True, null=True)
    firma = models.ImageField(upload_to='users_firmas',
                                        help_text=_('Maximo tamaño de imagen 8mb'), blank=True, null=True)
    foto = models.ImageField(upload_to='users_profiles',
                                        help_text=_('Maximo tamaño de imagen 8mb'), blank=True, null=True)

    state = models.ForeignKey('cities.State', on_delete=models.PROTECT , blank=True, null=True, verbose_name='Departamento')
    city = models.ForeignKey('cities.City', on_delete=models.PROTECT , blank=True, null=True, verbose_name='Ciudad')

    fundacion = models.ForeignKey(Fundacion, models.DO_NOTHING, db_column='fundacion', blank=True, null=True)

    def get_absolute_url(self):
        return reverse('users:user_profile', args=[self.id])

    def get_rol(self):
        return dict(self.TIPO_ROL).get(self.rol)

    def is_responsable(self):
        if dict(self.TIPO_ROL).get(self.rol)=='Responsable' :
            return True
        return False

    def nombre_completo(self):
        return str(self.first_name)+str(' ')+str(self.last_name)

    def __str__(self):
        return self.username

class Referencia(models.Model):
    tipo_identificacion = models.CharField(max_length=2,
                            choices=TIPO_ID,
                            blank=True)
    num_identificacion = models.CharField('Numero de identificación', max_length=10)
    nombre = models.CharField(max_length=250, blank=True, null=True)
    apellido = models.CharField(max_length=250, blank=True, null=True)
    telefono = models.CharField('Telefono de contacto',  max_length=30, blank=True)
    direccion = models.CharField(max_length=250, blank=True, null=True)
    email = models.EmailField(max_length=250, blank=True, null=True)
    postulante = models.ForeignKey(OwnerProfile, models.DO_NOTHING)
