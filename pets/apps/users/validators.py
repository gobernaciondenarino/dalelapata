from django.core.exceptions import ValidationError

#import re

def validate_facebook_url(value):
    msg = 'Por favor, Introduzca una URL valida de facebook.'
    if 'www.facebook.com/' not in value and 'www.fb.com' not in value:
        raise ValidationError(msg)
"""
def validate_numero_documento(value):
	# ^(([1-9]\.\d{3})|([1-9]|[1-9][1-9]))\.\d{3}\.\d{3}$
	reg = re.compile(' ^(([1-9]\.\d{3})|([1-9]|[1-9][1-9]))\.\d{3}\.\d{3}$')
    if !reg.match(value) :
        raise ValidationError(u'%s Por favor, Introduzca el numero de documento con los respectivos puntos' % value)
"""