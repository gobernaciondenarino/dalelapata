from django.shortcuts import render
from django.views.generic import TemplateView
from apps.meupet.views import render_pet_list,pet_list
from apps.meupet import forms, models
from django.views.generic import (ListView)

from django.shortcuts import render_to_response
from django.template import RequestContext

class AboutPageView(TemplateView):
    template_name = 'staticpages/about.html'
    context_object_name = 'pets'

class PetIndexHome(ListView):
    template_name = 'common/home.html'
    context_object_name = 'pets'

    def get_queryset(self):
        return models.Pet.objects.select_related('city').order_by('-id')[:12]

def handler404(request):
    response = render_to_response('404.html', {},
                                  context_instance=RequestContext(request))
    response.status_code = 404
    return response


def handler500(request):
    response = render_to_response('500.html', {},
                                  context_instance=RequestContext(request))
    response.status_code = 500
    return response
