import hashlib

from django.conf import settings
from django.urls import reverse
from django.db import models
from django.utils import timezone, crypto
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _

from autoslug import AutoSlugField
from django_extensions.db.models import TimeStampedModel

from apps.meupet import services
from apps.users.models import OwnerProfile, Fundacion
from apps.adopcion.models import TipoRelacion, Relacion

from django.core.validators import MaxValueValidator, MinValueValidator

     
class PetQuerySet(models.QuerySet):
    def _filter_by_kind(self, kind):
        try:
            return self.actives().filter(kind__id=int(kind)).select_related('city')
        except ValueError:
            return self.actives().filter(kind__slug=kind).select_related('city')

    def get_unpublished_pets(self):
        return self.filter(published=False)

    def get_staled_pets(self):
        """
        Pets considered as staled are not modified after a given
        number of days and don't have a request_sent date
        """
        stale_date = timezone.now() - timezone.timedelta(days=settings.DAYS_TO_STALE_REGISTER)
        return self.filter(
            modified__lt=stale_date,
            request_sent__isnull=True,
            status__in=PetStatus.objects.filter(final=False)
        )

    def actives(self):
        """
        Return only pets with active = True
        """
        return self.filter(active=True)

    def get_expired_pets(self):
        """Expired pets have request_sent date older than expected"""
        expire_date = timezone.now() - timezone.timedelta(days=settings.DAYS_TO_STALE_REGISTER)
        return self.filter(active=True, request_sent__lt=expire_date)


class KindManager(models.Manager):
    def count_pets(self, status):
        return self.filter(pet__status__in=status, pet__active=True) \
            .annotate(num_pets=models.Count('pet')).order_by('kind')


class Kind(models.Model):
    kind = models.TextField(max_length=100, unique=True)
    slug = AutoSlugField(max_length=30, populate_from='kind')

    objects = KindManager()

    def __str__(self):
        return self.kind


def get_slug(instance):
    city = ''
    if instance.city:
        city = instance.city.name
    return slugify('{}-{}'.format(instance.name, city))


class StatusGroup(models.Model):
    slug = AutoSlugField(max_length=64, unique=True, populate_from='name')
    name = models.CharField(max_length=64)

    def __str__(self):
        return self.name

class Raza(models.Model):
    nombre = models.CharField(max_length=64)
    especie = models.ForeignKey(Kind, on_delete=models.PROTECT, null=True, blank=True)

    def __str__(self):
        return self.nombre

class PetStatus(models.Model):
    group = models.ForeignKey(StatusGroup, related_name='statuses', blank=True, null=True, on_delete=models.PROTECT)
    code = models.CharField(max_length=32, unique=True)
    description = models.CharField(max_length=64)
    next_status = models.OneToOneField('self', blank=True, null=True, on_delete=models.PROTECT)
    final = models.BooleanField(default=False)

    class Meta:
        ordering = ('description',)

    def __str__(self):
        return self.description

    @property
    def next(self):
        if self.final == False:
            return self.next_status
        else :
            return self
        


class Pet(TimeStampedModel):
    MALE = 'MA'
    FEMALE = 'FE'
    PET_SEX = (
        (FEMALE, _('Hembra')),
        (MALE, _('Macho')),
    )
    SMALL = 'SM'
    MEDIUM = 'MD'
    LARGE = 'LG'
    PET_SIZE = (
        (SMALL, _('Pequeño')),
        (MEDIUM, _('Mediano')),
        (LARGE, _('Grande')),
    )
    BLANCO = '1'
    NEGRO = '2'
    AMARILLO = '3'
    CAFE = '4'
    OTRO = '5'
    COLOR = (
        (BLANCO, _('Blanco')),
        (NEGRO, _('Negro')),
        (AMARILLO, _('Dorado')),
        (CAFE, _('Cafe')),
        (OTRO, _('Otro')),
    )

    UNO = '1'
    DOS = '2'
    TRES = '3'
    CUATRO = '4'
    CINCO = '5'
    CONDICION = (
        (UNO, _('1 - Muy delgado')),
        (DOS, _('2 - Delgado')),
        (TRES, _('3 - Ideal')),
        (CUATRO, _('4 - Sobrepeso')),
        (CINCO, _('5 - Obeso')),
    )

    chip_id = models.BigIntegerField(unique=True, null=True, blank=True)
    condicion_corporal = models.CharField(max_length=1,
                            choices=CONDICION,
                            blank=True)
    peso = models.DecimalField(max_digits=5, decimal_places=2)
    color = models.CharField(max_length=250, null=True, blank=True)
    edad = models.CharField(max_length=250, null=True, blank=True)
    owner = models.ForeignKey(OwnerProfile, on_delete=models.PROTECT)
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=500)
    city = models.ForeignKey('cities.City', on_delete=models.PROTECT)
    kind = models.ForeignKey(Kind, null=True, on_delete=models.PROTECT )
    status = models.ForeignKey(PetStatus, related_name='pets', blank=True, null=True, on_delete=models.PROTECT)
    size = models.CharField(max_length=2,
                            choices=PET_SIZE,
                            blank=True)
    sex = models.CharField(max_length=2,
                           choices=PET_SEX,
                           blank=True)
    profile_picture = models.ImageField(upload_to='pet_profiles',
                                        help_text=_('Tamaño Maximo 8MB'))
    published = models.BooleanField(default=False)  # published on facebook
    request_sent = models.DateTimeField(null=True, blank=True)
    request_key = models.CharField(blank=True, max_length=40)
    active = models.BooleanField(default=True)
    slug = AutoSlugField(max_length=50, populate_from=get_slug, unique=True)
    fundacion = models.ForeignKey(Fundacion, models.DO_NOTHING, db_column='fundacion')
    raza = models.ForeignKey(Raza, models.DO_NOTHING, null=True, blank=True)

    objects = PetQuerySet.as_manager()

    def get_absolute_url(self):
        return reverse('meupet:detail', kwargs={'pk_or_slug': self.slug})

    def change_status(self):
        self.status = self.status.next
        self.save()

    def get_adoptante_actual(self):
        tipor = TipoRelacion.objects.get(nombre='Adopción')
        r = Relacion.objects.get(mascota=self , tipo_relacion = tipor)
        return r.usuario.nombre_completo()

    def is_found_or_adopted(self):
        return self.status.final

    def get_status(self):
        return self.status.description

    def get_sex(self):
        return dict(self.PET_SEX).get(self.sex)

    def get_condicion_corporal(self):
        return dict(self.CONDICION).get(self.condicion_corporal)

    def get_size(self):
        return dict(self.PET_SIZE).get(self.size)

    def request_action(self):
        hash_input = (crypto.get_random_string(5) + self.name).encode('utf-8')
        self.request_key = hashlib.sha1(hash_input).hexdigest()

        if not services.send_request_action_email(self):
            return

        self.request_sent = timezone.now()
        self.save(update_modified=False)

    def activate(self):
        self.request_sent = None
        self.request_key = ''
        self.active = True
        self.save()

    def deactivate(self):
        if not services.send_deactivate_email(self):
            return

        self.active = False
        self.save(update_modified=False)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-id']
        unique_together = ('name', 'owner')


class Photo(models.Model):
    pet = models.ForeignKey(Pet, on_delete=models.PROTECT)
    image = models.ImageField(upload_to='pet_photos')

from django.utils.timezone import now
class Achievement(models.Model):
    #REGISTER = 'RG'
    CHIPING = 'CH'
    STERILIZATION = 'ST'
    STATUS = (
        #(REGISTER, _('Registrado')),
        (CHIPING, _('Chipeado')),
        (STERILIZATION, _('Esterilizado')),
    )
    pet = models.ForeignKey(Pet, null=False, blank=False, on_delete=models.CASCADE)
    achievement = models.CharField(choices=STATUS, null=False, blank=False, max_length=2)
    creation_date = models.DateTimeField(default=now)