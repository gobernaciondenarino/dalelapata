from django.contrib import admin

from apps.meupet import models


class PetAdmin(admin.ModelAdmin):
    list_display = (
        'chip_id',
        'name',
        'kind',
        'city',
        'status',
        'fundacion',
        #'modified',
        'published',
        'request_sent',
        'active',
    )
    list_filter = (
        'fundacion',
        'status',
        'kind',
        'created',
        'active',
        'city__state',
    )

    search_fields = ['name', 'chip_id', ]

    #en caso de no ser superusuario solo mostrar las pets correspondientes a su fundacion
    def get_queryset(self, request):        
        query = super(PetAdmin, self).get_queryset(request)
        filtered_query = query.filter() 
        if not request.user.is_superuser :
            filtered_query = query.filter(fundacion=request.user.fundacion.id)  
            #exclude = ('is_superuser',)
        return filtered_query

class RazaAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'nombre',
        'especie',
    )
    list_filter = (
        'especie',
    )

    search_fields = ['nombre', ]

class PhotoAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'image',
        'pet',
    )

    search_fields = ['image', 'pet__name', 'pet__chip_id']

class PetStatusAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'code',
        'description',
        'next_status',
        'final',
    )

class AchievementAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'pet',
        'achievement',
    )

    #search_fields = ['image', 'pet__name', 'pet__chip_id', 'ddd']


admin.site.register(models.Pet, PetAdmin)
admin.site.register(models.Kind)
admin.site.register(models.Photo, PhotoAdmin)
admin.site.register(models.PetStatus, PetStatusAdmin)
admin.site.register(models.StatusGroup)
admin.site.register(models.Raza, RazaAdmin)
admin.site.register(models.Achievement, AchievementAdmin)
