from django import forms
from django.db.models import Q
from django.utils.translation import ugettext as _

from apps.meupet import models
from apps.cities.models import City, State
from apps.meupet.models import PetStatus


def _build_choice_field(label, choices=None, required=False):
    empty_choice = (('', '------------'),)
    field = forms.ChoiceField(
        widget=forms.Select(attrs={'class': 'form-control'}),
        label=label,
        choices=empty_choice,
        required=required
    )
    if choices:
        field.choices += choices
    return field


class PetForm(forms.ModelForm):
    city = _build_choice_field(_('City'), required=True)
    state = _build_choice_field(_('State'), required=True)
    status = _build_choice_field(_('Status'), required=False)

    class Meta:
        ult_id = models.Pet.objects.filter().first()
        
        if not ult_id:
            cont = 0
        else :
            cont = ult_id.id

        model = models.Pet
        fields = ('name', 'description', 'city', 'kind',
                  'profile_picture', 'size', 'sex', 'status','chip_id', 'edad', 'raza','color','condicion_corporal', 'peso')
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'value': _('Mascota '+str(cont+1))}),
            'chip_id': forms.TextInput(attrs={'class': 'form-control', 'placeholder': _('# chip en caso de tenerlo')}),
            'color': forms.TextInput(attrs={'class': 'form-control', 'placeholder': _('Describa colores predominantes')}),
            'edad': forms.TextInput(attrs={'class': 'form-control', 'placeholder': _('Edad aproximada o cualitativa')}),
            'description': forms.Textarea(attrs={
                'class': 'form-control',
                'placeholder': _(
                    "Describe la mascota"
                    "Aqui puedes relatar una historia, "
                    "rasgos particulares, entre otros aspectos")
            }),
            'kind': forms.Select(attrs={'class': 'form-control'}),
            'size': forms.Select(attrs={'class': 'form-control'}),
            'sex': forms.Select(attrs={'class': 'form-control'}),
            'raza': forms.Select(attrs={'class': 'form-control'}),
            #'condicion_corporal': forms.update(attrs={'class': 'form-control'}),
            #fields['tipo_identificacion'].widget.attrs.update({'class': 'form-control'})
            'condicion_corporal': forms.Select(attrs={'class': 'form-control', 'placeholder': _('Elija un rango de 1-5, optimo 3')}),
            'peso': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': _('Separador de decimal coma(,)'), 'step': 0.25}),
        }

    @staticmethod
    def _get_status_choices(current_status=None):
        status_filter = Q(final=False)
        if current_status:
            status_filter |= Q(id=current_status)
        queryset = models.PetStatus.objects.values_list('id', 'description').filter(status_filter)
        return queryset.order_by('final', 'description')

    def __init__(self, *args, **kwargs):
        initial = kwargs.pop('initial', {})
        instance = kwargs.get('instance')
        if instance:
            initial['state'] = instance.city.state.code
            initial['city'] = instance.city.code
            initial['status'] = instance.status.id
        kwargs['initial'] = initial
        super(PetForm, self).__init__(*args, **kwargs)

        self.fields['state'].choices += tuple(State.objects.values_list('code', 'name'))
        self.fields['status'].choices += tuple(self._get_status_choices(initial.get('status')))

        state = kwargs['initial'].get('state')
        if 'data' in kwargs:
            state = kwargs['data'].get('state')
        if state:
            self.fields['city'].choices += tuple(City.objects.filter(state__code=state).values_list('code', 'name'))

    def clean_profile_picture(self):
        img = self.cleaned_data.get('profile_picture', False)
        if img and img.size > 8 * 1024 * 1024:
            raise forms.ValidationError(_('Tamaño de imagen mayor que 8MB'))
        return img

    def clean_name(self):
        return self.cleaned_data['name'].title()

    def clean_city(self):
        return City.objects.get(code=self.cleaned_data['city'])

    def clean_status(self):
        st = self.cleaned_data['status']
        if not st:
            return None
        else:
            return PetStatus.objects.get(pk=st)

class EditPetForm(forms.ModelForm):
    city = _build_choice_field(_('City'), required=True)
    state = _build_choice_field(_('State'), required=True)
    status = _build_choice_field(_('Status'), required=False)

    class Meta:
        ult_id = models.Pet.objects.filter().first()
        
        if not ult_id:
            cont = 0
        else :
            cont = ult_id.id

        model = models.Pet
        fields = ('name', 'description', 'city', 'kind',
                  'profile_picture', 'size', 'sex', 'status','chip_id', 'edad', 'raza','color','condicion_corporal', 'peso')
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'value': _('Mascota '+str(cont+1))}),
            'chip_id': forms.TextInput(attrs={'class': 'form-control', 'placeholder': _('# chip en caso de tenerlo')}),
            'color': forms.TextInput(attrs={'class': 'form-control', 'placeholder': _('Describa colores predominantes')}),
            'edad': forms.TextInput(attrs={'class': 'form-control', 'placeholder': _('Edad aproximada o cualitativa')}),
            'description': forms.Textarea(attrs={
                'class': 'form-control',
                'placeholder': _(
                    "Describe la mascota"
                    "Aqui puedes relatar una historia, "
                    "rasgos particulares, entre otros aspectos")
            }),
            'kind': forms.Select(attrs={'class': 'form-control'}),
            'size': forms.Select(attrs={'class': 'form-control'}),
            'sex': forms.Select(attrs={'class': 'form-control'}),
            'raza': forms.Select(attrs={'class': 'form-control'}),
            #'condicion_corporal': forms.update(attrs={'class': 'form-control'}),
            #fields['tipo_identificacion'].widget.attrs.update({'class': 'form-control'})
            'condicion_corporal': forms.Select(attrs={'class': 'form-control', 'placeholder': _('Elija un rango de 1-5, optimo 3')}),
            'peso': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': _('Separador de decimal coma(,)'), 'step': 0.25}),
        }

    @staticmethod
    def _get_status_choices(current_status=None):
        status_filter = Q(final=False)
        if current_status:
            status_filter |= Q(id=current_status)
        queryset = models.PetStatus.objects.values_list('id', 'description').filter(status_filter)
        return queryset.order_by('final', 'description')

    def __init__(self, *args, **kwargs):
        initial = kwargs.pop('initial', {})
        instance = kwargs.get('instance')
        if instance:
            initial['state'] = instance.city.state.code
            initial['city'] = instance.city.code
            if instance.status:
                initial['status'] = instance.status.id
            else:
                instance.status = PetStatus.objects.get(pk=1)
                initial['status'] = instance.status
            
        kwargs['initial'] = initial
        super(PetForm, self).__init__(*args, **kwargs)

        self.fields['state'].choices += tuple(State.objects.values_list('code', 'name'))
        self.fields['status'].choices += tuple(self._get_status_choices(initial.get('status')))

        state = kwargs['initial'].get('state')
        if 'data' in kwargs:
            state = kwargs['data'].get('state')
        if state:
            self.fields['city'].choices += tuple(City.objects.filter(state__code=state).values_list('code', 'name'))

    def clean_profile_picture(self):
        img = self.cleaned_data.get('profile_picture', False)
        if img and img.size > 8 * 1024 * 1024:
            raise forms.ValidationError(_('Tamaño de imagen mayor que 8MB'))
        return img

    def clean_name(self):
        return self.cleaned_data['name'].title()

    def clean_city(self):
        return City.objects.get(code=self.cleaned_data['city'])

    def clean_status(self):
        st = self.cleaned_data['status']
        if not st:
            return None
        else:
            return PetStatus.objects.get(pk=st)


class SearchForm(forms.Form):
    chip_id = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': _('# chip en caso de tenerlo')}), required=False)
    city = _build_choice_field(_('City'))
    kind = _build_choice_field(_('Kind'))
    size = _build_choice_field(_('Size'), models.Pet.PET_SIZE)
    status = _build_choice_field(_('Status'))
    sex = _build_choice_field(_('Sex'), models.Pet.PET_SEX)

    def __init__(self, *args, **kwargs):
        super(SearchForm, self).__init__(*args, **kwargs)
        self.fields['city'].choices += tuple(
            City.objects.filter(pet__active=True).order_by('name').values_list('id', 'name').distinct()
        )
        self.fields['kind'].choices += tuple(models.Kind.objects.values_list('id', 'kind'))
        self.fields['status'].choices += tuple(
            models.PetStatus.objects.values_list('id', 'description').filter(final=False).order_by('description')
        )

    def clean(self):
        cleaned_data = super(SearchForm, self).clean()

        has_cleaned_filter = any([
            cleaned_data['chip_id'],
            cleaned_data['city'],
            cleaned_data['kind'],
            cleaned_data['sex'],
            cleaned_data['size'],
            cleaned_data['status'],
        ])

        if not has_cleaned_filter:
            raise forms.ValidationError(_('Debes seleccionar un filtro'))
