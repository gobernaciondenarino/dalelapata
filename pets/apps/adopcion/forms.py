from crispy_forms.helper import FormHelper
from django.utils.translation import ugettext_lazy as _
from crispy_forms.layout import Submit
from django import forms
from apps.users.models import OwnerProfile, Referencia
from .models import Seguimiento
class PostForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):
		super(PostForm, self).__init__(*args, **kwargs)

		self.helper = FormHelper()
		"""
		self.fields['tipo_identificacion'].required = True
		self.fields['num_identificacion'].required = True
		self.fields['first_name'].required = True
		self.fields['last_name'].required = True
		self.fields['email'].required = True
		self.fields['phone'].required = True

		self.fields['state'].required = True
		self.fields['city'].required = True

		self.fields['direccion'].required = True
		"""
		#atreem
		acento = "àèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÿÄËÏÖÜŸçÇßØøÅåÆæœ"

		self.fields['tipo_identificacion'].widget.attrs.update({'class': 'form-control'})
		self.fields['num_identificacion'].widget.attrs.update({'class': 'form-control'})
		self.fields['first_name'].widget.attrs.update({
			'class': 'form-control',
            'placeholder': _('Ej: Pepito , Pepito Fulano, Pepito F.'), 
            'pattern':'^[a-zA-z'+acento+']{3,15}(\s{1}([a-zA-z'+acento+']{3,15}|[a-zA-z'+acento+']{1,2}.)(\s{1}[a-zA-z'+acento+']{3,15})?)?$', 
            'title':'Ingrese nombres validos, Ej: Pepito '
		})
		self.fields['last_name'].widget.attrs.update({
			'class': 'form-control',
            'placeholder': _('Ej: Arcos , Arcos Torres, Arcos T.'), 
            'pattern':'^[a-zA-z'+acento+']{2,15}(\s{1}([a-zA-z'+acento+']{2,15}|[a-zA-z'+acento+']{1,2}.)(\s{1}[a-zA-z'+acento+']{2,15})?(\s{1}[a-zA-z'+acento+']{2,15})?)?$', 
            'title':'Ingrese apellidos validos, Ej: Arcos '
		})
		self.fields['email'].widget.attrs.update({'class': 'form-control'})
		self.fields['phone'].widget.attrs.update({'class': 'form-control'})

		self.fields['state'].widget.attrs.update({'class': 'form-control'})
		self.fields['city'].widget.attrs.update({'class': 'form-control'})

		self.fields['direccion'].widget.attrs.update({'class': 'form-control'})
		self.fields['facebook'].widget.attrs.update({'class': 'form-control'})

	class Meta:
		model = OwnerProfile
		fields = ('tipo_identificacion','num_identificacion','first_name', 'last_name','email','phone','direccion','state','city','facebook')

		widgets = {
            'num_identificacion': forms.TextInput(attrs={
                'class': 'form-control', 
                'placeholder': _('Ej: 1080232432, 50640123'), 
                'pattern':'^(([1-9]\d{3})|([1-9]|[1-9][0-9]))\d{3}\d{3}$', 
                'title':'Ingrese un numero de identificación valido sin espacios o puntos'
            }),
        } 
	"""
	def __init__(self, *args, **kwargs):
        initial = kwargs.pop('initial', {})
        instance = kwargs.get('instance')
        if instance:
            initial['state'] = instance.city.state.code
            initial['city'] = instance.city.code
            initial['status'] = instance.status.id
        kwargs['initial'] = initial
        super(PetForm, self).__init__(*args, **kwargs)

        self.fields['state'].choices += tuple(State.objects.values_list('code', 'name'))
        self.fields['status'].choices += tuple(self._get_status_choices(initial.get('status')))

        state = kwargs['initial'].get('state')
        if 'data' in kwargs:
            state = kwargs['data'].get('state')
        if state:
            self.fields['city'].choices += tuple(City.objects.filter(state__code=state).values_list('code', 'name'))	
	"""
class ReferenceForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):
		super(referenciaForm, self).__init__(*args, **kwargs)

		self.helper = FormHelper()
	
		acento = "àèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÿÄËÏÖÜŸçÇßØøÅåÆæœ"

		self.fields['tipo_identificacion'].widget.attrs.update({'class': 'form-control'})
		self.fields['num_identificacion'].widget.attrs.update({'class': 'form-control'})
		self.fields['nombre'].widget.attrs.update({
			'class': 'form-control',
            'placeholder': _('Ej: Pepito , Pepito Fulano, Pepito F.'), 
            'pattern':'^[a-zA-z'+acento+']{3,15}(\s{1}([a-zA-z'+acento+']{3,15}|[a-zA-z'+acento+']{1,2}.)(\s{1}[a-zA-z'+acento+']{3,15})?)?$', 
            'title':'Ingrese nombres validos, Ej: Pepito '
		})
		self.fields['apellido'].widget.attrs.update({
			'class': 'form-control',
            'placeholder': _('Ej: Arcos , Arcos Torres, Arcos T.'), 
            'pattern':'^[a-zA-z'+acento+']{2,15}(\s{1}([a-zA-z'+acento+']{2,15}|[a-zA-z'+acento+']{1,2}.)(\s{1}[a-zA-z'+acento+']{2,15})?(\s{1}[a-zA-z'+acento+']{2,15})?)?$', 
            'title':'Ingrese apellidos validos, Ej: Arcos '
		})
		self.fields['telefono'].widget.attrs.update({'class': 'form-control'})
		self.fields['direccion'].widget.attrs.update({'class': 'form-control'})

		self.fields['email'].widget.attrs.update({'class': 'form-control'})

	class Meta:
		model = Referencia
		fields = ('tipo_identificacion','num_identificacion','nombre', 'apellido','telefono','direccion','email')

		widgets = {
            'num_identificacion': forms.TextInput(attrs={
                'class': 'form-control', 
                'placeholder': _('Ej: 1080232432, 50640123'), 
                'pattern':'^(([1-9]\d{3})|([1-9]|[1-9][0-9]))\d{3}\d{3}$', 
                'title':'Ingrese un numero de identificación valido sin espacios o puntos'
            }),
        } 


class ContratoForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):
		super(ContratoForm, self).__init__(*args, **kwargs)
		self.helper = FormHelper()
		
		#self.fields['estado'].required = True
		self.fields['fecha'].required = True
		#self.fields['descripcion'].required = True
		self.fields['observaciones'].required = True
		#atreem

		#self.fields['tipo'].widget.attrs.update({'class': 'form-control'})
		#self.fields['tipo'].widget.attrs['selected'] = 3
		#self.fields['tipo'].widget.attrs['disabled'] = 'disabled'

		#self.fields['estado'].widget.attrs.update({'class': 'form-control'})
		self.fields['fecha'].widget.attrs.update({'class': 'form-control'})
		#self.fields['descripcion'].widget.attrs.update({'class': 'form-control'})
		self.fields['observaciones'].widget.attrs.update({'class': 'form-control'})

	class Meta:
		model = Seguimiento
		fields = ('fecha','observaciones')
		#fields = ('first_name', 'last_name', 'email',)
		#fields['first_name'].widget.attrs.update({'class': 'form-control'})
        #self.fields['last_name'].widget.attrs.update({'class': 'form-control'})
        #self.fields['email'].widget.attrs.update({'class': 'form-control'})	

class AdjuntoForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):
		super(AdjuntoForm, self).__init__(*args, **kwargs)
		self.helper = FormHelper()
		
		#self.fields['estado'].required = True
		#self.fields['fecha'].required = True
		self.fields['titulo'].required = True
		self.fields['observaciones'].required = True
		#atreem

		#self.fields['tipo'].widget.attrs.update({'class': 'form-control'})
		#self.fields['tipo'].widget.attrs['selected'] = 3
		#self.fields['tipo'].widget.attrs['disabled'] = 'disabled'

		#self.fields['estado'].widget.attrs.update({'class': 'form-control'})
		self.fields['titulo'].widget.attrs.update({'class': 'form-control'})
		#self.fields['descripcion'].widget.attrs.update({'class': 'form-control'})
		self.fields['observaciones'].widget.attrs.update({'class': 'form-control'})

	class Meta:
		model = Seguimiento
		fields = ('titulo','observaciones')
		#fields = ('first_name', 'last_name', 'email',)
		#fields['first_name'].widget.attrs.update({'class': 'form-control'})
        #self.fields['last_name'].widget.attrs.update({'class': 'form-control'})
        #self.fields['email'].widget.attrs.update({'class': 'form-control'})