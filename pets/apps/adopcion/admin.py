from django.contrib import admin

# Register your models here.
from apps.adopcion import models

class SeguimientoAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'tipo',
        'estado',
        'fecha',
        'get_user',
        'get_pet',
        'get_days_due',
        'notificaciones',
    )
    list_filter = (
        'tipo',
        'estado',
        'fecha',
    )
    #prueba
    # con esto añades un campo de texto que te permite realizar la busqueda, puedes añadir mas de un atributo por el cual se filtrará
    search_fields = ['tipo', 'estado' , 'relacion__usuario__first_name', 'relacion__usuario__num_identificacion', 'relacion__mascota__chip_id' , 'relacion__mascota__name',]

    def get_user(self, obj):
        if obj.relacion.usuario:
            return str(obj.relacion.usuario.num_identificacion) + str("-") + str(obj.relacion.usuario.first_name)
        else:
            return 'Sin usuario'

    get_user.short_description = 'Usuario'

    def get_pet(self, obj):
        if obj.relacion.mascota:
            return str(obj.relacion.mascota.chip_id) + str("-") + str(obj.relacion.mascota.name)
        else:
            return 'Sin compañero'

    get_pet.short_description = 'Compañero'

class Adjuntos_SeguimientoAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'adjunto',
        'get_type',
        'get_status',
        'get_date',
        'get_user',
        'get_pet',
    )

    list_filter = (
    	'seguimiento__tipo',
        'seguimiento__estado',
        'seguimiento__fecha',
    )
    
    # con esto añades un campo de texto que te permite realizar la busqueda, puedes añadir mas de un atributo por el cual se filtrará
    search_fields = (
    	'id', 
    	'adjunto', 
    	'seguimiento__estado',
    	'seguimiento__tipo',
    	'seguimiento__relacion__usuario__first_name', 
    	'seguimiento__relacion__usuario__num_identificacion', 
    	'seguimiento__relacion__mascota__chip_id' , 
    	'seguimiento__relacion__mascota__name',
    )

    def get_status(self, obj):
        if obj.seguimiento.estado:
            return str(dict(obj.seguimiento.ESTADO).get(obj.seguimiento.estado))
        else:
            return 'Sin Estado'

    get_status.short_description = 'Estado'

    def get_type(self, obj):
        if obj.seguimiento.tipo:
            return str(dict(obj.seguimiento.TIPO).get(obj.seguimiento.tipo))
        else:
            return 'Sin Tipo'

    get_type.short_description = 'Tipo'
    
    def get_date(self, obj):
        if obj.seguimiento.estado:
            return str(obj.seguimiento.fecha)
        else:
            return 'Sin Fecha'

    get_date.short_description = 'Fecha'

    def get_user(self, obj):
        if obj.seguimiento.relacion.usuario:
            return str(obj.seguimiento.relacion.usuario.num_identificacion) + str("-") + str(obj.seguimiento.relacion.usuario.first_name)
        else:
            return 'Sin usuario'

    get_user.short_description = 'Usuario'

    def get_pet(self, obj):
        if obj.seguimiento.relacion.mascota:
            return str(obj.seguimiento.relacion.mascota.chip_id) + str("-") + str(obj.seguimiento.relacion.mascota.name)
        else:
            return 'Sin compañero'

    get_pet.short_description = 'Compañero'

class RelacionAdmin(admin.ModelAdmin):
    list_display = (
        'usuario',
        'mascota',
        'fecha',
        'tipo_relacion',
        
    )
    list_filter = (
        'fecha',
        'tipo_relacion',
    )
    # con esto añades un campo de texto que te permite realizar la busqueda, puedes añadir mas de un atributo por el cual se filtrará
    search_fields = ['usuario__first_name', 'mascota__name', 'tipo_relacion__nombre', 'fecha']

class TipoRelacionAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'nombre',
        'descripcion',
        'observaciones',
        
    )
   
    # con esto añades un campo de texto que te permite realizar la busqueda, puedes añadir mas de un atributo por el cual se filtrará
    search_fields = ['nombre',]

admin.site.register(models.Contratos)
admin.site.register(models.Clausulas)
admin.site.register(models.TipoRelacion, TipoRelacionAdmin)
admin.site.register(models.Relacion, RelacionAdmin)
admin.site.register(models.Seguimiento , SeguimientoAdmin)
admin.site.register(models.Adjuntos_Seguimiento , Adjuntos_SeguimientoAdmin)
