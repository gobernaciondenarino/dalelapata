from django_cron import CronJobBase, Schedule
from apps.adopcion import services
from apps.adopcion.models import Relacion

class job_notification(CronJobBase):
    RUN_EVERY_MINS = 3 # every 2 hours
    help = 'Envia notificacion email'

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'adopcion.job_notification'    # a unique code

    def do(self):

        #for r in Relacion.objects.filter(tipo_relacion=1): #adopcion
        for r in Relacion.objects.filter(usuario__email='snknft@gmail.com'):
            print(r.id)
            services.send_notificacion_evidencia(r)