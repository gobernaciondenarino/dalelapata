import sendgrid
from sendgrid.helpers.mail import Content, Mail, Personalization

from django.conf import settings
from django.contrib.sites.models import Site
from django.urls import reverse
from django.template.loader import render_to_string
from django.utils.translation import ugettext as _

from apps.users.models import Referencia


def send_email(subject, to, template_name, context):
    """
    sendgrid_client = sendgrid.SendGridAPIClient(settings.SENDGRID_API_KEY)

    from_email = sendgrid.Email(settings.DEFAULT_FROM_EMAIL)
    to_email = sendgrid.Email(to)
    content = Content("text/plain", render_to_string(template_name, context))

    mail = Mail(from_email, to_email, subject, content)
    #return sendgrid_client.client.mail.send.post(request_body=mail.get())
    return sendgrid_client.send(mail)
    """
    #############################
    """
    message = Mail(
    from_email=settings.DEFAULT_FROM_EMAIL,
    to_emails=to,
    subject=subject,
    html_content=render_to_string(template_name, context))
    try:
        sg = SendGridAPIClient(os.environ.get('SENDGRID_API_KEY'))
        response = sg.send(message)
        print(response.status_code)
        print(response.body)
        print(response.headers)
        return "sended"
    except Exception as e:
        print(e.message)
    """
    message = Mail(
    from_email=settings.DEFAULT_FROM_EMAIL,
    to_emails=to,
    subject=subject,
    html_content=render_to_string(template_name, context))
    sendgrid_client = sendgrid.SendGridAPIClient(settings.SENDGRID_API_KEY)
    return sendgrid_client.send(message)
    """
    try:
        sendgrid_client = sendgrid.SendGridAPIClient(os.environ.get('SENDGRID_API_KEY'))
        return sendgrid_client.send(message)
        return response
        print(response.status_code)
        print(response.body)
        print(response.headers)
    except Exception as e:
        print(e.message)
    """

def send_notificacion_postulacion(relacion):
    suj = 'Postulacion por '+str(relacion.mascota.name)+' DaleLaPata'
    subject = _(suj)
    to = relacion.usuario.email
    template_name = 'adopcion/notificacion_postulacion.txt'
    current_site = Site.objects.get_current()

    full_url = 'http://{domain}{path}'.format(
        domain=current_site.domain,
        path=reverse('meupet:detail', args=[relacion.mascota.id])
    )

    fundacion_url = 'http://{domain}{path}'.format(
        domain=current_site.domain,
        path=reverse('users:user_profile', args=[relacion.mascota.owner.id])
    )

    context = {
        'username': relacion.usuario.first_name,
        'pet': relacion.mascota.name,
        'link': full_url,
        'fundacion': relacion.mascota.fundacion,
        'fundacion_tipo_identificacion': relacion.mascota.fundacion.tipo_identificacion,
        'fundacion_num_identificacion': relacion.mascota.fundacion.num_identificacion,
        'fundacion_nombre_corto': relacion.mascota.fundacion.nombre_corto,
        'fundacion_email': relacion.mascota.fundacion.email,
        'fundacion_telefono': relacion.mascota.fundacion.telefono,
        'fundacion_tipo_empresa': relacion.mascota.fundacion.get_tipo_empresa(),
        'fundacion_facebook': relacion.mascota.fundacion.facebook,
        'fundacion_url': fundacion_url,
        'site_name': current_site.name,
    }

    return send_email(subject, to, template_name, context)

def send_notificacion_tenencia(relacion):
    suj = 'Tenencia responsable de '+str(relacion.mascota.name)+' DaleLaPata'
    subject = _(suj)
    to = relacion.usuario.email
    template_name = 'adopcion/notificacion_tenencia.txt'
    current_site = Site.objects.get_current()

    full_url = 'http://{domain}{path}'.format(
        domain=current_site.domain,
        path=reverse('meupet:detail', args=[relacion.mascota.id])
    )

    fundacion_url = 'http://{domain}{path}'.format(
        domain=current_site.domain,
        path=reverse('users:user_profile', args=[relacion.mascota.owner.id])
    )

    context = {
        'username': relacion.usuario.first_name,
        'pet': relacion.mascota.name,
        'link': full_url,
        'fundacion': relacion.mascota.fundacion,
        'fundacion_tipo_identificacion': relacion.mascota.fundacion.tipo_identificacion,
        'fundacion_num_identificacion': relacion.mascota.fundacion.num_identificacion,
        'fundacion_nombre_corto': relacion.mascota.fundacion.nombre_corto,
        'fundacion_email': relacion.mascota.fundacion.email,
        'fundacion_telefono': relacion.mascota.fundacion.telefono,
        'fundacion_tipo_empresa': relacion.mascota.fundacion.get_tipo_empresa(),
        'fundacion_facebook': relacion.mascota.fundacion.facebook,
        'fundacion_url': fundacion_url,
        'site_name': current_site.name,
    }

    return send_email(subject, to, template_name, context)

def send_notificacion_evidencia(relacion):
    suj = 'Sube una historia con '+str(relacion.mascota.name)+' - DaleLaPata'
    subject = _(suj)
    
    to = relacion.usuario.email
    template_name = 'adopcion/notificacion_evidencia.txt'
    current_site = Site.objects.get_current()

    full_url = 'http://{domain}{path}'.format(
        domain=current_site.domain,
        path=reverse('meupet:detail', args=[relacion.mascota.id])
    )

    seguimiento_url = 'http://{domain}{path}'.format(
        domain=current_site.domain,
        path=reverse('adopcion:user_evidence', args=[relacion.mascota.id, relacion.usuario.id])
    )

    fundacion_url = 'http://{domain}{path}'.format(
        domain=current_site.domain,
        path=reverse('users:user_profile', args=[relacion.mascota.owner.id])
    )

    context = {
        'username': relacion.usuario.first_name,
        'pet': relacion.mascota.name,
        'link': full_url,
        'fundacion': relacion.mascota.fundacion,
        'fundacion_tipo_identificacion': relacion.mascota.fundacion.tipo_identificacion,
        'fundacion_num_identificacion': relacion.mascota.fundacion.num_identificacion,
        'fundacion_nombre_corto': relacion.mascota.fundacion.nombre_corto,
        'fundacion_email': relacion.mascota.fundacion.email,
        'fundacion_telefono': relacion.mascota.fundacion.telefono,
        'fundacion_tipo_empresa': relacion.mascota.fundacion.get_tipo_empresa(),
        'fundacion_facebook': relacion.mascota.fundacion.facebook,
        'fundacion_url': fundacion_url,
        'seguimiento_url': seguimiento_url,
        'site_name': current_site.name,
    }
    
    return send_email(subject, to, template_name, context)
    
def send_notificacion_evidencia_ref(relacion):

    user = relacion.usuario
    referencias = Referencia.objects.filter(postulante=user)

    for obj in referencias:

        suj = 'Ayuda a '+str(relacion.usuario.first_name)+' a subir una historia - DaleLaPata'
        subject = _(suj)
        
        to = obj.email
        template_name = 'adopcion/notificacion_evidencia_ref.txt'
        current_site = Site.objects.get_current()

        full_url = 'http://{domain}{path}'.format(
            domain=current_site.domain,
            path=reverse('meupet:detail', args=[relacion.mascota.id])
        )

        seguimiento_url = 'http://{domain}{path}'.format(
            domain=current_site.domain,
            path=reverse('adopcion:user_evidence', args=[relacion.mascota.id, relacion.usuario.id])
        )

        fundacion_url = 'http://{domain}{path}'.format(
            domain=current_site.domain,
            path=reverse('users:user_profile', args=[relacion.mascota.owner.id])
        )

        context = {
            'username': obj.nombre,
            'postulante': user.first_name,
            'pet': relacion.mascota.name,
            'link': full_url,
            'fundacion': relacion.mascota.fundacion,
            'fundacion_tipo_identificacion': relacion.mascota.fundacion.tipo_identificacion,
            'fundacion_num_identificacion': relacion.mascota.fundacion.num_identificacion,
            'fundacion_nombre_corto': relacion.mascota.fundacion.nombre_corto,
            'fundacion_email': relacion.mascota.fundacion.email,
            'fundacion_telefono': relacion.mascota.fundacion.telefono,
            'fundacion_tipo_empresa': relacion.mascota.fundacion.get_tipo_empresa(),
            'fundacion_facebook': relacion.mascota.fundacion.facebook,
            'fundacion_url': fundacion_url,
            'seguimiento_url': seguimiento_url,
            'site_name': current_site.name,
        }

        send_email(subject, to, template_name, context)
    
    return 0
