from django.conf.urls import url
from django.views.generic import RedirectView
from django.contrib.auth.views import LogoutView

from . import views

urlpatterns = [    
    url(r'^(?P<pk>[-\w]*)/$', views.post_new, name='create'),
    url(r'^logros$', views.logros, name='logros'),
    url(r'^tenure_create/(?P<pk>[-\w]*)/$', views.responsible_tenure, name='tenure_create'),
    url(r'^contratos/(?P<id>[-\w]*)/foto/$', views.upload_image, name='upload_image'),
    url(r'^contratos/(?P<m>(\d+))/(?P<u>(\d+))/reference/$', views.reference_add, name='reference_add'),
    #url(r'^check/(?P<e>[\w.%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4})$', views.checkusername, name='check'),
    url(r'^check/(?P<string>.*)$', views.checkusername, name='check'),
    url(r'^evidence/(?P<m>(\d+))/(?P<u>(\d+))/$', views.AdjuntarSeguimiento, name='user_evidence'),
    url(r'^sendnoti_evidence/(?P<relacion>(\d+))/(?P<t>(\d+))/$', views.notify_evidence, name='sendnoti_evidence'),

    #url(r'^$', views.post_new, name='create'),
]
