from .forms import PostForm, AdjuntoForm, ReferenceForm
from django.shortcuts import get_object_or_404 , render
from apps.meupet.models import Pet
from apps.adopcion.models import Relacion, TipoRelacion, Seguimiento , Adjuntos_Seguimiento
from apps.users.models import OwnerProfile , Referencia
import logging
from django.utils import timezone
from django.http import Http404, HttpResponseRedirect, HttpResponse, JsonResponse
from django.urls import reverse
from django.db import IntegrityError
from django.contrib import messages
from django.utils.translation import ugettext as _

from django.views.generic import CreateView, TemplateView, UpdateView, DetailView

from apps.adopcion import services

from rest_framework import generics
#from api import serializers
from rest_framework.views import APIView

from django.core import serializers

def post_new(request, pk):
    log=logging.getLogger(__name__)
    pet = Pet.objects.get(id=int(pk))

    if request.method == "POST":

        form = PostForm(request.POST)    
        if form.is_valid():
           
            ownerprofile = form.save(commit=False)
            u2 = OwnerProfile.objects.filter(email=ownerprofile.email).first()
            flag = 0

            if not u2:
                ownerprofile.username=ownerprofile.email
                ownerprofile.set_password("12345")
                ownerprofile.is_active = False
                ownerprofile.save()
                
            else :
                num_id = form.cleaned_data['num_identificacion']
                if num_id != u2.num_identificacion:
                    messages.info(request, _("El numero de identificación no coincide con el registrado en la plataforma, intentelo nuevamente"))
                    return HttpResponseRedirect(reverse('adopcion:create' , args=(pet.id,)))
                else:
                    ownerprofile = u2   
                    flag = 1     
                
            tipor = TipoRelacion.objects.get(nombre='Postulación')            

            relacion=Relacion()
            relacion.usuario=ownerprofile
            relacion.mascota = pet
            relacion.fecha = timezone.now()
            relacion.tipo_relacion = tipor            

            try:
                relacion.save()
                services.send_notificacion_postulacion(relacion)
            except IntegrityError:
                messages.info(request, _("Ya se ha postulado anteriormente a esta mascota,Sigue descrubriendo nuevos amigos"))
                return HttpResponseRedirect(reverse('adopcion:create' , args=(pet.id,)))

            s = Seguimiento.objects.create(
            tipo=3,
            estado=4,
            fecha=timezone.now(),
            titulo='Postulación para mascota, pendiente de celebrar contrato',
            relacion=relacion)
            
            s.save()
            #OwnerProfile.first_name = request.user
            #OwnerProfile.last_name = timezone.now()
            #OwnerProfile.last_name = timezone.now()
            #return redirect('post_detail', pk=post.pk)
            log.info("ENTRO")
            #context['fundacion'] = self.request.user.fundacion
            return render(request, 'adopcion/done.html', {'pet': pet , 'user_name': ownerprofile.nombre_completo,'form': form , 'flag': flag})
        else:
            return render(request, 'adopcion/home.html', {'form': form})
           
    else:
        form = PostForm()
        return render(request, 'adopcion/home.html', {'pet_name': pet.name, 'form': form})

def responsible_tenure(request, pk):
    log=logging.getLogger(__name__)
    pet = Pet.objects.get(id=int(pk))

    if request.method == "POST":

        form = PostForm(request.POST)    
        if form.is_valid():
           
            ownerprofile = form.save(commit=False)
            u2 = OwnerProfile.objects.filter(email=ownerprofile.email).first()
            flag = 0

            if not u2:
                ownerprofile.username=ownerprofile.email
                ownerprofile.set_password("12345")
                ownerprofile.is_active = False
                ownerprofile.save()
                
            else :
                num_id = form.cleaned_data['num_identificacion']
                if num_id != u2.num_identificacion:
                    messages.info(request, _("El numero de identificación no coincide con el registrado en la plataforma, intentelo nuevamente"))
                    return HttpResponseRedirect(reverse('adopcion:tenure_create' , args=(pet.id,)))
                else:
                    ownerprofile = u2   
                    flag = 1     
                
            tipor = TipoRelacion.objects.get(nombre='Tenencia responsable')            

            relacion=Relacion()
            relacion.usuario=ownerprofile
            relacion.mascota = pet
            relacion.fecha = timezone.now()
            relacion.tipo_relacion = tipor            

            try:
                relacion.save()
                services.send_notificacion_tenencia(relacion)
            except IntegrityError:
                messages.info(request, _("Ya te encontrabas registrado como teniente responsable de este amiguito"))
                return HttpResponseRedirect(reverse('adopcion:tenure_create' , args=(pet.id,)))

            s = Seguimiento.objects.create(
            tipo=1,
            estado=2,
            fecha=timezone.now(),
            titulo='Tenencia responsable',
            relacion=relacion)
            
            s.save()
            #OwnerProfile.first_name = request.user
            #OwnerProfile.last_name = timezone.now()
            #OwnerProfile.last_name = timezone.now()
            #return redirect('post_detail', pk=post.pk)
            log.info("ENTRO")
            #context['fundacion'] = self.request.user.fundacion
            return render(request, 'adopcion/tenure_done.html', {'pet': pet , 'user_name': ownerprofile.nombre_completo,'form': form , 'flag': flag})
        else:
            return render(request, 'adopcion/tenure_form.html', {'form': form})
           
    else:
        form = PostForm()
        return render(request, 'adopcion/tenure_form.html', {'pet_name': pet.name, 'form': form})

def reference_add(request, pk, m):
    #log=logging.getLogger(__name__)
    ownerprofile = OwnerProfile.objects.get(id=int(pk))
    pet = Pet.objects.get(id=int(m))

    if request.method == "POST":

        form = referenciaForm(request.POST)    
        if form.is_valid():
           
            referencia = form.save(commit=False)
            referencia.postulante = ownerprofile           

            try:
                 referencia.save()  
            except IntegrityError:
                messages.info(request, _("Error al crear referencia"))
                return HttpResponseRedirect(reverse('users:c_new', kwargs={'m': pet.id, 'u': ownerprofile.id}))

            log.info("ENTRO")
            #context['fundacion'] = self.request.user.fundacion
            return HttpResponseRedirect(reverse('users:c_new', kwargs={'m': pet.id, 'u': ownerprofile.id}))
        else:
            return HttpResponseRedirect(reverse('users:c_new', kwargs={'m': pet.id, 'u': ownerprofile.id}))
           
    else:
        form = ReferenceForm()
        return HttpResponseRedirect(reverse('users:c_new', kwargs={'m': pet.id, 'u': ownerprofile.id}))

def upload_image(request, id):
    r = get_object_or_404(Relacion, id=id)
    s = get_object_or_404(Seguimiento, relacion=r.id, anterior__isnull=True)
    picture = request.FILES.get('another_picture', False)

    if request.method == 'POST' and picture:
        Adjuntos_Seguimiento.objects.create(seguimiento=s, adjunto=picture)

    return HttpResponseRedirect(reverse('users:c_new', kwargs={'m': r.mascota.id, 'u': r.usuario.id}))

def checkusername(request, string):
    #email = request.POST.get('email', False)
    email = string
    if email:
        u = OwnerProfile.objects.filter(email=email).count()
        if u != 0:
            res = "1"
            q = OwnerProfile.objects.filter(email=email).first()
            #para enviar todo el objeto en formato json, nota: quitar el first en la consulta
            #data = serializers.serialize("json", q)
            #return HttpResponse(data)
            data = {
                'nombre': q.nombre_completo(),
                'tipo': q.tipo_identificacion,
                'documento': q.num_identificacion,
            }
            return JsonResponse(data)
        else:
            res = "0"
    else:
        res = "-1"

    return HttpResponse('%s' % res)

##############################################
def AdjuntarSeguimiento(request, m, u):
    relacion=Relacion.objects.get(mascota=m, usuario=u)
    seguimiento=Seguimiento.objects.get(relacion=relacion)
    #seguimiento=Seguimiento()

    if request.method == "POST":

        form = AdjuntoForm(request.POST)    
        
        if form.is_valid():
            #seguimiento = form.save(commit=False)  
            #seguimiento.descripcion = form.cleaned_data['descripcion']
            

            seguimiento.observaciones = form.cleaned_data['observaciones']
            seguimiento.titulo = form.cleaned_data['titulo']
            #seguimiento.fecha = form.cleaned_data['fecha']    
            seguimiento.relacion = relacion
            seguimiento.tipo = 1
            seguimiento.estado = 4
            seguimiento.save()
           
            return render(request, 'adopcion/cargar_done.html', {'relacion':relacion , 'seguimiento':seguimiento })
        else:
            return render(request, 'adopcion/cargar_evidencia.html', {'form': form , 'relacion':relacion , 'seguimiento':seguimiento})
           
    else:
        #form = ContratoForm()
        #form.fields['descripcion'].widget.attrs.update({'value': 'hola hola'})
        form = AdjuntoForm()
        return render(request, 'adopcion/cargar_evidencia.html', {'form': form , 'relacion':relacion, 'seguimiento':seguimiento})
##############################################
def notify_evidence(request, relacion, t):
    relacion2=Relacion.objects.get(id=relacion)

    if t=='0':
        services.send_notificacion_evidencia(relacion2)
    else:
        services.send_notificacion_evidencia_ref(relacion2)
    
    seguimiento2 = Seguimiento.objects.get(relacion=relacion2)
    seguimiento2.notify_add()
    return HttpResponseRedirect(reverse('users:a_list'))
###################################################################
from apps.meupet.models import Pet, Achievement
def logros(request):
    res = 0
    for pet in Pet.objects.all():
        if pet.chip_id:
            Achievement.objects.create(
               pet = pet,
               achievement = 'CH'
            )
            res = res + 1

    return HttpResponse('%s' % res)
        