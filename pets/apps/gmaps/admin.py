from django.contrib import admin

# Register your models here.
from apps.gmaps.models import Categoria, Ubicacion, Tabcatub

admin.site.register(Categoria)
admin.site.register(Ubicacion)
admin.site.register(Tabcatub)
