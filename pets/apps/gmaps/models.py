from django.db import models
#from django.contrib.auth.models import User
from apps.users.models import OwnerProfile
from django.utils.translation import ugettext_lazy as _


MARK = True
UNMARK = False
VISIT = (
    (MARK, _('Visitado')),
    (UNMARK, _('Pendiente')),
)

class Categoria(models.Model):
	nombre=models.CharField(max_length=200, null=False, blank=False)

	def __str__(self):
		return '{}'.format(self.nombre)


class Ubicacion(models.Model):
	nombre=models.CharField(max_length=200, null=False, blank=False)
	lat=models.CharField(max_length=50, null=False, blank=False)
	lng=models.CharField(max_length=50, null=False, blank=False)
	fecha=models.DateTimeField(auto_now_add=True)
	cantidad=models.IntegerField(default=0)
	categorias=models.ManyToManyField(Categoria, through='Tabcatub', through_fields=('location', 'category'), null=False, blank=False)
	estado=models.BooleanField(choices=VISIT, null=False, blank=False)
	fecha_inicial=models.CharField(max_length=50, null=True, blank=True)
	fecha_final=models.CharField(max_length=50, null=True, blank=True)
	foto = models.ImageField(upload_to='fotos', blank=True, null=True)
	user=models.ForeignKey(OwnerProfile,on_delete=True)


	
class Tabcatub(models.Model):
	category = models.ForeignKey(Categoria, on_delete=models.CASCADE)
	location = models.ForeignKey(Ubicacion, on_delete=models.CASCADE)  


