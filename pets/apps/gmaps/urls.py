from django.conf.urls import url, include
from apps.gmaps.views import Mapa_Ubicacion, sitios, ubicacion_edit, ubicacion_delete, mapMapbox, Crear_Categoria, categoria_edit, categoria_delete

urlpatterns = [
    url(r'^mapaubicar$',Mapa_Ubicacion, name='Mapa_Ubicacion'),    
    url(r'^puntosMarcados$',sitios, name='sitios'),
    url(r'^editar/(?P<id_ubicacion>(\d+))/(?P<estado>([\w]+))/$', ubicacion_edit, name='ubicacion_edit'),
    url(r'^eliminar/(?P<id_ubicacion>\d+)/$', ubicacion_delete, name='ubicacion_delete'),
    url(r'^mapabox$',mapMapbox, name='mapMapbox'),    
    url(r'^categoria$',Crear_Categoria, name='Crear_Categoria'),
    url(r'^editarc/(?P<id_categoria>\d+)/$', categoria_edit, name='categoria_edit'),
    url(r'^eliminarc/(?P<id_categoria>\d+)/$', categoria_delete, name='categoria_delete'),
    
    
    
]