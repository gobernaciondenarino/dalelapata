from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from apps.gmaps.forms import GmapsForm, CategoriaForm
from apps.gmaps.models import Ubicacion, Categoria, Tabcatub
from django.core import serializers
from django.core.paginator import Paginator
from django.template import RequestContext
import json


def sitios(request):
	return render(request,'gmaps/mapaPuntos.html',  {
		'puntos':serializers.serialize('json', Ubicacion.objects.all()), 
		'categs':serializers.serialize('json', Categoria.objects.all()),
		'catubs':serializers.serialize('json', Tabcatub.objects.all())
		})

def Mapa_Ubicacion(request):
	if request.method == 'POST':		
		form = GmapsForm(request.POST, request.FILES)
		if form.is_valid():
			form.save()			
		return redirect('gmaps:Mapa_Ubicacion')	
	else:
		form = GmapsForm()		
		
	return render(request,'gmaps/mapaUbicar.html',{'form':form, 'listaQ':serializers.serialize('json', Ubicacion.objects.all())})


def ubicacion_edit(request, id_ubicacion, estado):
	ubicacion=Ubicacion.objects.get(id=id_ubicacion)
	if estado=='VISITADO':
		edi='/static/img/visitado.png'
	else:
		edi='/static/img/pendiente.png'	
	if request.method=='GET':
		form=GmapsForm(instance=ubicacion);
	else:
		form = GmapsForm(request.POST, instance=ubicacion)
		if form.is_valid():
			form.save()
		return redirect('gmaps:Mapa_Ubicacion')
	return render(request,'gmaps/mapaUbicar.html',{'form':form, 'edi':edi})

def ubicacion_delete(request, id_ubicacion):
	ubicacion=Ubicacion.objects.get(id=id_ubicacion)
	if request.method=='POST':
		ubicacion.delete()
		return redirect('gmaps:Mapa_Ubicacion')
	return render(request,'gmaps/ubicacion_delete.html',{'ubicacion':ubicacion})

def mapMapbox(request):
	return render(request,'base/pruebaMapbox.html')

def normalize(s):
    replacements = (
        ("á", "a"),
        ("é", "e"),
        ("í", "i"),
        ("ó", "o"),
        ("ú", "u"),
    )
    for a, b in replacements:
        s = s.replace(a, b).replace(a.upper(), b.upper())
    return s

def check_name_cat(name_cat):
	nam_cat=normalize(name_cat)
	categorys=Categoria.objects.all()
	exists=False
	for category in categorys:
		nombrecat=normalize(category.nombre)
		if nam_cat.lower()==nombrecat.lower():
			exists=True
	return exists

def Crear_Categoria(request):
	listcats=Categoria.objects.all().order_by('id')
	if request.method == 'POST':
		formc = CategoriaForm(request.POST)		
		if formc.is_valid():
			elnombre=formc.cleaned_data['nombre'] 		
			if not check_name_cat(elnombre):
				formc.save()
		return redirect('gmaps:Crear_Categoria')	
	else:
		formc = CategoriaForm()	

	paginator=Paginator(listcats,15)
	page=request.GET.get('page')
	listcats=paginator.get_page(page)
	return render(request,'gmaps/creaCategoria.html',{'formc':formc, 'listcats':listcats, 'listaC':serializers.serialize('json', Categoria.objects.all())})


def categoria_edit(request,id_categoria):
	idcat=id_categoria
	categori=Categoria.objects.get(id=id_categoria)
	if request.method=='GET':
		formc=CategoriaForm(instance=categori);
	else:
		formc = CategoriaForm(request.POST, instance=categori)
		if formc.is_valid():
			elnombre=formc.cleaned_data['nombre'] 		
			if not check_name_cat(elnombre):
				formc.save()
		return redirect('gmaps:Crear_Categoria')
	return render(request,'gmaps/creaCategoria.html',{'formc':formc,'idcat':idcat})


def verificar_cat(id_cat):
	ubicat=Tabcatub.objects.all()
	vinculos=False
	for ub in ubicat:
		if id_cat==ub.category:
			vinculos=True
	return vinculos


def categoria_delete(request, id_categoria):
	sepuede=True
	categori=Categoria.objects.get(id=id_categoria)
	if not verificar_cat(categori):
		if request.method=='POST':
			categori.delete()
			return redirect('gmaps:Crear_Categoria')
	else:	
		sepuede=False
	return render(request,'gmaps/categoria_delete.html',{'categori':categori, 'sepuede':sepuede})	










