from django import forms
#from django.forms import ModelForm
from apps.gmaps.models import Ubicacion, Categoria


class CategoriaForm(forms.ModelForm):

	class Meta:
		model = Categoria
		
		fields = [
			'nombre',
		]
		labels = {
			'nombre':'Nombre',
		}
		widgets = {
			'nombre':forms.TextInput(attrs={'class':'form-control', 'id':'nom'}),
		}



class GmapsForm(forms.ModelForm):

	class Meta:
		model = Ubicacion

		fields = [
			'nombre',
			'lat',
			'lng',
			'cantidad',
			'categorias',
			'estado',
			'fecha_inicial',
			'fecha_final',
			'foto',
			'user',
		]

		labels = {
			'nombre':'Nombre',
			'lat':'Latitud',
			'lng':'Longitud',			
			'cantidad':'Cantidad',
			'categorias':'Categoria',
			'estado':'Estado',
			'fecha_inicial':'Fecha Inicial',
			'fecha_final':'Fecha final',
			'foto':'Foto',
			'user':'Usuario',
		}
	
		widgets = {			
			'nombre':forms.TextInput(attrs={'class':'form-control'}),	
			'lat':forms.HiddenInput(),
			'lng':forms.HiddenInput(),		
			'cantidad':forms.NumberInput(attrs={'class':'form-control'}),
			'categorias':forms.CheckboxSelectMultiple(attrs={'class':'nomcats'}),
			'estado':forms.Select(attrs={'class':'form-control','id':'estadio'}),		
			'fecha_inicial':forms.TextInput(attrs={'class':'form-control','id':'proxv','placeholder':'Fecha inicial'}),
			'fecha_final':forms.TextInput(attrs={'class':'form-control','id':'fecf','placeholder':'Fecha final'}),
			'foto':forms.ClearableFileInput(attrs={'class':'form-control','id':'fot','placeholder':'Ingresa imagen visita'}),
			'user':forms.Select(attrs={'class':'form-control'}),		
		}



