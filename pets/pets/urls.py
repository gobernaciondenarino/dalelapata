from django.conf import settings
from django.conf.urls import include, url, static
from django.contrib import admin
from django.views.static import serve

#app_name = 'meupet'
#urlpatterns = [
    #url(r'^pets/', include('meupet.urls', namespace='meupet')),
#    url(r'^pets/', include(('meupet.urls', 'meupet'), namespace='meupet')),
    #path(r'^pets/', include('meupet.urls', namespace='meupet')),
#    url(r'^social/', include('social_django.urls', namespace='social')),
#    url(r'^user/', include('users.urls', namespace='users')),
#    url(r'^api/', include('api.urls', namespace='api')),
#    url(r'^admin/', include(admin.site.urls)),
#    url(r'^', include('common.urls', namespace='common')),
#
#]
admin.autodiscover()
urlpatterns = [
    url(r'^pets/', include(('apps.meupet.urls', 'meupet'), namespace='meupet')),
    url(r'^social/', include(('social_django.urls', 'social'), namespace='social')),
    url(r'^user/', include(('apps.users.urls', 'users'), namespace='users')),
    url(r'^api/', include(('api.urls', 'api'), namespace='api')),
    url(r'^admin/', admin.site.urls),
    url(r'^', include(('apps.common.urls', 'common'), namespace='common')),
    url(r'^adopcion/', include(('apps.adopcion.urls', 'adopcion'), namespace='adopcion')),
    url(r'^gmaps/', include(('apps.gmaps.urls', 'gmaps'), namespace="gmaps")),
    
    url(r'^media/(?P<path>.*)$', serve,{'document_root': settings.MEDIA_ROOT}),
    url(r'^static/(?P<path>.*)$', serve,{'document_root': settings.STATIC_ROOT}),
]



if settings.DEBUG:
    urlpatterns += static.static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
